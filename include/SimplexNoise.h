#pragma once

#include <boost/numeric/ublas/matrix.hpp>
#include "WhiteNoise.h"
#include "Interpolation.h"
#include <float.h>

using namespace boost::numeric::ublas;

// 1D _____
class SimplexNoise1D : public WhiteNoise1D
{
public:
	SimplexNoise1D(uint16_t w = 512,
				   uint16_t seed = 0,
				   bool isSigned = false,
				   float persistence = 0.5f,
				   uint16_t octaves = 3,
				   Interpolation::InterpolationType iType = Interpolation::Cosine);
	~SimplexNoise1D(void);

	float makeNoise(vector<float> *heightMap);

private:
	float m_persistence;
	uint16_t m_octaves;
	Interpolation::InterpolationType m_iType;
};

// 2D _____
class SimplexNoise2D : public WhiteNoise2D
{
public:
	SimplexNoise2D(uint16_t w = 512,
				 uint16_t h = 512,
				 uint16_t seed = 0,
				 bool isSigned = false,
				 float persistence = 0.5f,
				 uint16_t octaves = 3,
				 Interpolation::InterpolationType iType = Interpolation::Cosine);
	~SimplexNoise2D(void);

	float makeNoise(matrix<float> *heightMap);

private:
	float m_persistence;
	uint16_t m_octaves;
	Interpolation::InterpolationType m_iType;
};

