#pragma once

#include "Common.h"
#include <float.h>

#include <boost/numeric/ublas/matrix.hpp>
#include <random>
#include "glm/glm.hpp"

#include "Interpolation.h"

using namespace boost::numeric::ublas;

// 1D _____
class MidpointDisplacementNoise1D
{
public:
	MidpointDisplacementNoise1D(uint16_t width = 512,
								uint16_t seed = 0,
								bool isSigned = false,
								float hardness = 0.5f);
	~MidpointDisplacementNoise1D(void);

	GeneratorStatus makeNoise(vector<float> *heightMap);

private:
	vector<float> *m_heightMap;

	bool m_isSigned;
	float m_maxVal,
		  m_minVal;
};

// 2D _____
class MidpointDisplacementNoise2D
{
public:
	MidpointDisplacementNoise2D(uint16_t width = 512,
								uint16_t height = 512,
								uint16_t seed = 0,
								bool isSigned = false,
								float hardness = 0.5f);
	~MidpointDisplacementNoise2D(void);

	GeneratorStatus makeNoise(matrix<float> *heightMap);

private:
	matrix<float> *m_heightMap;

	bool m_isSigned;
	float m_maxVal,
		  m_minVal;
};
