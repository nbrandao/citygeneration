#pragma once


#include "Common.h"

#include <stdint.h>

#include "vslibs.h"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/unordered_map.hpp>

#include "ValueNoise.h"
#include "PerlinNoise.h"
#include "MidpointDisplacementNoise.h"

#include "Erosion.h"
#include "Interpolation.h"

#define M_FOREST_ELEMENT_MAX_RADIUS			2.0f
#define M_FOREST_ELEMENT_OUTER_BORDER		2.0f
#define M_TREE_AMOUNT						5

using namespace boost::numeric::ublas;

class Terrain
{
public:
	Terrain();
	~Terrain(void);

	class NoiseSettings
	{
	public:
		typedef enum
		{
			VALUE_LOW_NOISE,
			VALUE_AVERAGE_NOISE,
			VALUE_HIGH_NOISE,

			PERLIN_LOW_NOISE,
			PERLIN_AVERAGE_NOISE,
			PERLIN_HIGH_NOISE,

			MIDPOINT_DISPLACEMENT_LOW_NOISE,
			MIDPOINT_DISPLACEMENT_AVERAGE_NOISE,
			MIDPOINT_DISPLACEMENT_HIGH_NOISE,
		} PredefinedNoise;

		enum Density { VERY_LOW = 1, LOW = 2, AVERAGE = 3, HIGH = 4, VERY_HIGH = 5, };
		// Cell noise may be easily added to the list below in the future
		typedef enum { Value,  Displacement, Gradient, } NoiseType;
		typedef enum { Perlin, /* Simplex, Wavelet, */ } GradientNoise;
		typedef enum { MidpointDisplacement, /* DiamondSquare, */ } DisplacementNoise;
		typedef Interpolation::InterpolationType InterpolationType;

		NoiseSettings(uint16_t seed, Terrain::NoiseSettings::PredefinedNoise predefinedNoise)
		{
			this->m_seed = seed;

			switch (predefinedNoise)
			{
				case Terrain::NoiseSettings::PredefinedNoise::VALUE_LOW_NOISE:
					this->m_lacunarity = 1.7f;
					this->m_gain = 0.5f;
					this->m_octaves = 5;
					this->m_interpolationType = Terrain::NoiseSettings::InterpolationType::Cubic;

					this->m_noiseType = NoiseSettings::NoiseType::Value;
					break;
				case Terrain::NoiseSettings::PredefinedNoise::VALUE_AVERAGE_NOISE:
					this->m_lacunarity = 2.0f;
					this->m_gain = 0.7f;
					this->m_octaves = 6;
					this->m_interpolationType = Terrain::NoiseSettings::InterpolationType::Cubic;

					this->m_noiseType = NoiseSettings::NoiseType::Value;
					break;
				case Terrain::NoiseSettings::PredefinedNoise::VALUE_HIGH_NOISE:
					this->m_lacunarity = 2.0f;
					this->m_gain = 0.8f;
					this->m_octaves = 7;
					this->m_interpolationType = Terrain::NoiseSettings::InterpolationType::SmoothStep;

					this->m_noiseType = NoiseSettings::NoiseType::Value;
					break;

				case Terrain::NoiseSettings::PredefinedNoise::PERLIN_LOW_NOISE:
					this->m_lacunarity = 1.7f;
					this->m_gain = 0.5f;
					this->m_octaves = 5;
					this->m_interpolationType = Terrain::NoiseSettings::InterpolationType::Cubic;

					this->m_noiseType = NoiseSettings::NoiseType::Value;
					break;
				case Terrain::NoiseSettings::PredefinedNoise::PERLIN_AVERAGE_NOISE:
					this->m_lacunarity = 2.0f;
					this->m_gain = 0.7f;
					this->m_octaves = 6;
					this->m_interpolationType = Terrain::NoiseSettings::InterpolationType::Cubic;

					this->m_noiseType = NoiseSettings::NoiseType::Value;
					break;
				case Terrain::NoiseSettings::PredefinedNoise::PERLIN_HIGH_NOISE:
					this->m_lacunarity = 2.0f;
					this->m_gain = 0.8f;
					this->m_octaves = 7;
					this->m_interpolationType = Terrain::NoiseSettings::InterpolationType::Cosine;

					this->m_noiseType = NoiseSettings::NoiseType::Value;
					break;

				case Terrain::NoiseSettings::PredefinedNoise::MIDPOINT_DISPLACEMENT_LOW_NOISE:
					this->m_displacementType = Terrain::NoiseSettings::DisplacementNoise::MidpointDisplacement;
					this->m_hardness = 0.90f;

					this->m_noiseType = NoiseSettings::NoiseType::Displacement;
					break;
				case Terrain::NoiseSettings::PredefinedNoise::MIDPOINT_DISPLACEMENT_AVERAGE_NOISE:
					this->m_displacementType = Terrain::NoiseSettings::DisplacementNoise::MidpointDisplacement;
					this->m_hardness = 0.94f;

					this->m_noiseType = NoiseSettings::NoiseType::Displacement;
					break;
				case Terrain::NoiseSettings::PredefinedNoise::MIDPOINT_DISPLACEMENT_HIGH_NOISE:
					this->m_displacementType = Terrain::NoiseSettings::DisplacementNoise::MidpointDisplacement;
					this->m_hardness = 0.97f;

					this->m_noiseType = NoiseSettings::NoiseType::Displacement;
					break;
			}
		}

		NoiseSettings(uint16_t seed = 0,
					  float lacunarity = 2.0f,
					  float gain = 0.5f,
					  uint16_t octaves = 5,
					  NoiseSettings::InterpolationType interpolationType = NoiseSettings::InterpolationType::Cosine)
		{
			this->m_seed = seed;
			this->m_lacunarity = lacunarity;
			this->m_gain = gain;
			this->m_octaves = octaves;
			this->m_interpolationType = interpolationType;

			this->m_noiseType = NoiseSettings::NoiseType::Value;
		}

		NoiseSettings(uint16_t seed = 0,
					  NoiseSettings::GradientNoise noiseType = NoiseSettings::GradientNoise::Perlin,
					  float lacunarity = 2.0f,
					  float gain = 0.5f,
					  uint16_t octaves = 5,
					  NoiseSettings::InterpolationType interpolationType = NoiseSettings::InterpolationType::Cosine)
		{
			this->m_seed = seed;
			this->m_gradientType = noiseType;
			this->m_lacunarity = lacunarity;
			this->m_gain = gain;
			this->m_octaves = octaves;
			this->m_interpolationType = interpolationType;

			this->m_noiseType = NoiseSettings::NoiseType::Gradient;
		}

		NoiseSettings(uint16_t seed = 0,
					  NoiseSettings::DisplacementNoise noiseType = NoiseSettings::DisplacementNoise::MidpointDisplacement,
					  float hardness = 0.8f)
		{
			this->m_displacementType = noiseType;
			this->m_hardness = hardness;

			this->m_noiseType = NoiseSettings::NoiseType::Displacement;
		}

		~NoiseSettings() {}

		uint16_t getSeed() { return this->m_seed; }
		NoiseSettings::NoiseType getType() { return this->m_noiseType; }
		NoiseSettings::GradientNoise getGradientType() { return this->m_gradientType; }
		NoiseSettings::DisplacementNoise getDisplacementType() { return this->m_displacementType; }
		NoiseSettings::InterpolationType getInterpolationType() { return this->m_interpolationType; }
		float getLacunarity() { return this->m_lacunarity; }
		float getGain() { return this->m_gain; }
		float getHardness() { return this->m_hardness; }
		uint16_t getOctaves() { return this->m_octaves; }

	private:
		uint16_t m_seed;
		NoiseSettings::NoiseType m_noiseType;
		NoiseSettings::GradientNoise m_gradientType;
		NoiseSettings::DisplacementNoise m_displacementType;
		NoiseSettings::InterpolationType m_interpolationType;
		float m_lacunarity,
			  m_gain,
			  m_hardness;
		uint16_t m_octaves;
	};

	class ErosionSettings
	{
	public:
		typedef enum { Thermal, /* Hidraulic = Erosion::ErosionType::Hydraulic, */ } ErosionType;
		typedef enum { Standard = Erosion::ErosionType::Thermal, Improved = Erosion::ErosionType::Improved, } ThermalErosion;

		ErosionSettings(Terrain::ErosionSettings::ThermalErosion thermalErosionType = Terrain::ErosionSettings::ThermalErosion::Standard,
						float talus = 1.0f,
						uint16_t iterations = 3)
		{
			this->m_thermalType = thermalErosionType;
			this->m_talus = talus;
			this->m_iterations = iterations;

			this->m_erosionType = Terrain::ErosionSettings::ErosionType::Thermal;
		}

		~ErosionSettings() {}

		Terrain::ErosionSettings::ErosionType getErosionType() { return this->m_erosionType; }
		Terrain::ErosionSettings::ThermalErosion getThermalErosionType() { return this->m_thermalType; }
		uint16_t getIterations() { return this->m_iterations; }
		float getTalus() { return this->m_talus; }

	private:
		ErosionSettings::ErosionType m_erosionType;
		ErosionSettings::ThermalErosion m_thermalType;
		uint16_t m_iterations;
		float m_talus;
	};

	class BufferData
	{
	public:
		BufferData(float *positions, int positionsAmount, float *normals, int normalsAmount)
		{
			this->posCoords = positions;
			this->posAmount = positionsAmount;
			this->normCoords = normals;
			this->normAmount = normalsAmount;
		}
		~BufferData() {}

		float *posCoords,
			  *normCoords;
		int posAmount,
			normAmount;
	};

	// ________________________________________________________________________________________
	// Initialization and modelling

	GeneratorStatus init(uint16_t seed = 0,
						 bool isSigned = true,
						 uint16_t xw = 1024,
						 uint16_t zw = 1024,
						 float xz_grid = 1.0f,
						 float y_height = 250.0f);

	GeneratorStatus genHeightMap(NoiseSettings *settings = 0);

	GeneratorStatus erode(ErosionSettings *settings = 0);

	BufferData getBuffers(GeneratorStatus *status = 0);

	// ________________________________________________________________________________________
	// Allocation

	class Candidate
	{
	public:
		Candidate(glm::vec3 c = glm::vec3(0.0f, 0.0f, 0.0f), float r = 10.0f, float o = 15.0f)
		{
			this->center = c;
			this->innerRadius = r;
			this->outerRadius = o;
		}
		~Candidate() {}

		glm::vec3 center;
		float innerRadius,
			  outerRadius;
	};
	std::vector<VSResModelLib> m_trees;
	std::vector<Candidate> m_tree_space;
	std::map<Candidate *, VSResModelLib *> m_forest;

	std::map<Candidate *, VSResModelLib *> *getForest();

	std::vector<Candidate> getCityCandidates(uint16_t seed = 0,
											 GeneratorStatus *status = 0);

	std::vector<Candidate> getForestCandidates(uint16_t seed = 0,
											   Terrain::NoiseSettings::Density density = (Terrain::NoiseSettings::Density)0,
											   GeneratorStatus *status = 0);

	GeneratorStatus setupCity(std::vector<Candidate>);

	GeneratorStatus setupForest(uint16_t seed = 0,
								std::vector<Candidate> = std::vector<Candidate>());

	GeneratorStatus setupRiver(Terrain::NoiseSettings *settings = 0);

	// ________________________________________________________________________________________

	GeneratorStatus generateTotallyRandom(uint16_t seed,
										  uint16_t widthX,
										  uint16_t widthZ,
										  uint16_t heightY);

private:
	typedef enum
	{
		City = 1,
		Forest = 2,
		River = 3,
	} RegionType;

	void wheighedPolarDistanceXY(RegionType type, uint16_t *x, float *y, uint16_t *z, glm::vec3 center, float innerRadius, float outerRadius);
	void wheighedLinearDistanceX(RegionType type, uint16_t *x, float *y, uint16_t *z, uint16_t width, float point, float margin);
	bool detectPolarColision(matrix<uint16_t> *, uint16_t *x, uint16_t *y, float *radius);

	matrix<float> *m_heightMap;
	matrix<uint16_t> *m_regionMask;

	bool m_isSigned,
		 m_terrainSet,
		 m_noiseSet,
		 m_citySet,
		 m_riverSet,
		 m_forestSet;

	float m_terrainHeight,
		  m_terrainXZGrid;

	class Vertex
	{
	public:
		Vertex() {}
		~Vertex() {}
		glm::vec3 position;
		glm::vec3 normal;
	};

	vector<float> *m_positions;
	vector<float> *m_normals;
	vector<float> *m_textures;
};
