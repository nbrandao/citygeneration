#pragma once

#include "Common.h"
#include <float.h>

#include <boost/numeric/ublas/matrix.hpp>
#include <random>

#include "Interpolation.h"

using namespace boost::numeric::ublas;

class WhiteNoise1D
{
public:
	WhiteNoise1D(uint16_t w = 512,
				 uint16_t seed = 0,
				 bool isSigned = false);
	~WhiteNoise1D(void);

	vector<float> *getNoise(void);
	uint16_t getWidth(void);

protected:
	float evaluate(Interpolation::InterpolationType iType, float x);
	float getValue(int x);

private:
	vector<float> *m_randomVector;
};

class WhiteNoise2D
{
public:
	WhiteNoise2D(uint16_t w = 512,
				 uint16_t h = 512,
				 uint16_t seed = 0,
				 bool isSigned = false);
	~WhiteNoise2D(void);

	matrix<float> *getNoise(void);
	uint16_t getWidth(void);
	uint16_t getHeight(void);

protected:
	float evaluate(Interpolation::InterpolationType iType, float x, float y);
	float getValue(int x, int y);

private:
	matrix<float> *m_randomMatrix;
};

