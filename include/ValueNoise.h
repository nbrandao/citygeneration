#pragma once

#include "Common.h"
#include "WhiteNoise.h"
#include <float.h>

#include <boost/numeric/ublas/matrix.hpp>
#include <random>
#include "glm/glm.hpp"

#include "Interpolation.h"

using namespace boost::numeric::ublas;

// 1D _____
class ValueNoise1D : public WhiteNoise1D
{
public:
	ValueNoise1D(uint16_t width = 512,
				 uint16_t seed = 0,
				 bool isSigned = false,
				 float lacunarity = 2.0f,
				 float gain = 0.5f,
				 uint16_t octaves = 3,
				 Interpolation::InterpolationType iType = Interpolation::Cosine);
	~ValueNoise1D(void);

	GeneratorStatus makeNoise(vector<float> *heightMap);

private:
	// @Override
	float evaluate(Interpolation::InterpolationType iType, float x);

	vector<float> *m_heightMap;

	bool m_isSigned;
	float m_maxVal,
		  m_minVal;
};

// 2D _____
class ValueNoise2D : public WhiteNoise2D
{
public:
	ValueNoise2D(uint16_t width = 512,
				 uint16_t height = 512,
				 uint16_t seed = 0,
				 bool isSigned = false,
				 float lacunarity = 2.0f,
				 float gain = 0.5f,
				 uint16_t octaves = 3,
				 Interpolation::InterpolationType iType = Interpolation::Cosine);
	~ValueNoise2D(void);

	GeneratorStatus makeNoise(matrix<float> *heightMap);

private:
	// @Override
	float evaluate(Interpolation::InterpolationType iType, float x, float y);

	matrix<float> *m_heightMap;

	bool m_isSigned;
	float m_maxVal,
		  m_minVal;
};
