#ifndef __StreetGen__
#define __StreetGen__

#include <time.h>
#include <vector>
#include <queue>
#include "CityRandom.h"
#include "Settings.h"
#include "vslibs.h"
#include "Vertex.h"

class StreetGen
{
public:
    StreetGen();
    StreetGen(float x, float y, float z, float radius);

public:

    struct ProposedStreet
    {
        int time;
        Vertex *org;
        Vertex *dst;
        int direction;
        VSResBufferLib *street;
        int orientation;
        int type;
        int total_length;
        bool intersect;

        ProposedStreet(){
            time = 0;
            org = new Vertex();
            dst = new Vertex();
            direction = FRONT;
            street = new VSResBufferLib();
            orientation = 0;
        }

        ProposedStreet(int init_time, Vertex *init_org, Vertex *init_dst, int init_dic,int init_orientation, int init_type, int init_total_length){
            time = init_time;
            org = init_org;
            dst = init_dst;
            direction = init_dic;
            street = new VSResBufferLib();
            orientation = init_orientation;
            type = init_type;
            total_length = init_total_length;
            intersect = false;
        }
    };

    struct StreetGenComparison
    {
        bool operator()( const StreetGen::ProposedStreet* s1, const StreetGen::ProposedStreet* s2) const;
    };

    std::priority_queue<ProposedStreet *, std::vector<ProposedStreet *>, StreetGenComparison> queue;
    std::vector<ProposedStreet *> streets;

    enum street_dir {FRONT = 0, LEFT = 1, RIGHT = 2, BACK = 3};
    enum street_type {MAIN = 0, AVENUE = 1};

    int genCounter;

    float center_x;
    float center_y;
    float center_z;
    float radius;

    void generate();
    bool localConstraints(ProposedStreet *ps);
    std::vector<ProposedStreet *> globalGoals(ProposedStreet ps);
    bool loadStreet(ProposedStreet *ps);

        // Random Stuff
    std::mt19937 mt;

private:
    ProposedStreet* generateMainStreet(ProposedStreet from);
    ProposedStreet* generateAvenueFromMain(ProposedStreet main);
    ProposedStreet* generateAvenue(ProposedStreet from);
    int invertDirection(int direction);
    int newDirection(int direction, int orientation);
    bool intercept(Vertex P, Vertex Q, Vertex E, Vertex F);
    bool parallel(ProposedStreet street, ProposedStreet matched);
    bool isVertical(int direction);
};
#endif
