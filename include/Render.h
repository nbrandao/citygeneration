#ifndef __Render__
#define __Render__

#include "vslibs.h"
#include "Vertex.h"
#include "Terreno.h"
#include "StreetGen.h"
#include "BuildingGen.h"
#include "Terrain.h"

extern VSMathLib *vsml;
extern bool next;

// State Vars
enum STATE {TERRAIN, STREETS, BUILDINGS, DONE};

void preloadBuffers();
void render();
void renderTrees();
void renderStreets();
void renderBuildings();

#endif // !__Render__