#pragma once

#include "Common.h"

#include <cmath>

class Interpolation
{
public:
	static Interpolation *getInstance(void);

	typedef enum
	{
		Linear,
		Cosine,
		Cubic,
		SmoothStep,
	} InterpolationType;

	float linearInterpolate(float a, float b, float t);
	float cosineInterpolate(float a, float b, float t);
	float cubicInterpolate(float a, float b, float c, float d, float t);
	float smoothStep(float a, float b, float t);

private:
	Interpolation(void);
	~Interpolation(void);

	static Interpolation *instance;
};