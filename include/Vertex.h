#ifndef __VERTEX__
#define __VERTEX__

struct Vertex {
	float x;
	float y;
	float z;

	Vertex(){
		x = 0;
		y = 0;
		z = 0;
	}

	Vertex(float init_x, float init_y, float init_z){
		x = init_x;
		y = init_y;
		z = init_z;
	}
};

#endif // !__VERTEX__