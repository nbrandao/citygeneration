#ifndef __BuildingGen__
#define __BuildingGen__

#include <iostream>
#include <vector>
#include <boost/numeric/ublas/matrix.hpp>
#include "Vertex.h"
#include "StreetGen.h"


class BuildingGen
{
	public:
		BuildingGen();
		BuildingGen(StreetGen streets);

	public:
		struct Building {
			Vertex *org_corner;
			Vertex *dst_corner;
			float height;
			VSResBufferLib *building;

			Building(Vertex *init_org_corner, Vertex *init_dst_corner, float init_height){
				org_corner = init_org_corner;
				dst_corner = init_dst_corner;
				height = init_height;
				building = new VSResBufferLib();
			}
		};

		boost::numeric::ublas::matrix<int> overlay;
		StreetGen m_city;

		std::vector<Building *> buildings;

		void generateBuildings(StreetGen city);
		bool canBuild(int org_x, int org_z, int dst_x, int dst_z);
		void updateOverlay(int org_x, int org_z, int dst_x, int dst_z);

		bool loadBuilding(Building *b);
};


#endif