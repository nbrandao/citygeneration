#pragma once

#include "Common.h"
#include <float.h>

#include <boost/numeric/ublas/matrix.hpp>
#include <random>
#include <algorithm>
#include "glm/glm.hpp"

#include "Interpolation.h"

#define M_GRADIENT_ANGLE	30

using namespace boost::numeric::ublas;

// 1D _____
class PerlinNoise1D
{
public:
	PerlinNoise1D(uint16_t width = 512,
				  uint16_t seed = 0,
				  bool isSigned = false,
				  float lacunarity = 2.0f,
				  float gain = 0.5f,
				  uint16_t octaves = 3,
				  Interpolation::InterpolationType iType = Interpolation::Cosine);
	~PerlinNoise1D(void);

	GeneratorStatus makeNoise(vector<float> *heightMap);

private:
	// @Override
	float evaluate(Interpolation::InterpolationType, float x);

	glm::vec2 gradientX(uint16_t x);
	vector<glm::vec2> *m_gradients;
	vector<uint16_t> *m_permutations;

	vector<float> *m_heightMap;

	bool m_isSigned;
	float m_maxVal,
		  m_minVal;
};

// 2D _____
class PerlinNoise2D
{
public:
	PerlinNoise2D(uint16_t width = 512,
				  uint16_t height = 512,
				  uint16_t seed = 0,
				  bool isSigned = false,
				  float lacunarity = 2.0f,
				  float gain = 0.5f,
				  uint16_t octaves = 3,
				  Interpolation::InterpolationType iType = Interpolation::Cosine);
	~PerlinNoise2D(void);

	GeneratorStatus makeNoise(matrix<float> *heightMap);

private:
	// @Override
	float evaluate(Interpolation::InterpolationType, float x, float y);

	glm::vec2 gradientXY(uint16_t x, uint16_t y);
	vector<glm::vec2> *m_gradients;
	vector<uint16_t> *m_permutations;

	matrix<float> *m_heightMap;

	bool m_isSigned;
	float m_maxVal,
		  m_minVal;
};
