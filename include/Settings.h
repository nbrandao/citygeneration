#ifndef __SETTINGS__
#define __SETTINGS__

#include <unordered_map>
#include <string>

class Settings
{
	public:
		static Settings& get()
		{
			static Settings instance;
			return instance;
		}
		float getValue(std::string key);

	private:
		Settings();
		Settings(const Settings&);
		Settings& operator=(const Settings&);

		std::unordered_map <std::string, float> configs;
};


#endif