#pragma once

#include "Common.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <float.h>

using namespace boost::numeric::ublas;

class Erosion
{
public:
	static Erosion *getInstance(void);

	typedef enum
	{
		Thermal,
		Improved,
		Hydraulic,
	} ErosionType;

	GeneratorStatus thermalErode(matrix<float> *heightMap, uint16_t iterations, float talus);
	//GeneratorStatus hydraulicErode();
	GeneratorStatus improvedErode(matrix<float> *heightMap, uint16_t iterations, float talus);

private:
	Erosion(void);
	~Erosion(void);

	static Erosion *instance;
};

