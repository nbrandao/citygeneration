#pragma once

#include "./glm/glm.hpp"

#ifndef M_PI
#define M_PI	3.14159f
#endif

#define M_RANDOM_SIZE						512

enum GeneratorStatus
{
	GEN_NO_ERROR = 0,
	GEN_TERRAIN_ALREADY_ALLOCATED,
	GEN_TERRAIN_NOT_YET_ALLOCATED,
	GEN_HEIGHTMAP_ALREADY_CREATED,
	GEN_HEIGHTMAP_NOT_YET_CREATED,
	GEN_CITIES_NOT_YET_ALLOCATED,
	GEN_CITIES_ALREADY_ALLOCATED,
	GEN_RIVER_NOT_YET_ALLOCATED,
	GEN_RIVER_ALREADY_ALLOCATED,
	GEN_FOREST_ALREADY_ALLOCATED,
	GEN_SIZE_TOO_SMALL,
	GEN_NULL_POINTER,
	GEN_BAD_VALUE,
	GEN_FUNCTION_UNAVAILABLE,
};

float vec2dotProduct(glm::vec2 v1, glm::vec2 v2);

glm::vec3 vec3crossProduct(glm::vec3 n1, glm::vec3 n2);

glm::vec3 vec3normalize(glm::vec3 n);