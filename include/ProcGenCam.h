#pragma once

#include "glm/glm.hpp"

#ifndef M_PI
#define M_PI	3.14159f
#endif // !M_PI

enum Axis {
    X, Y, Z             // only for convenience
};
class ProcGenCam {
public:
    float pos[3];     // xyz position where camera is located
    float dir[3];     // xyz position where camera is looked at
    float gazeV[3];   // view vector
    float rightV[3];  // right vector
    float upV[3];     // up vector
    ProcGenCam(){
        pos[X] = 0;
        pos[Y] = 250;
        pos[Z] = -1000;
        dir[X] = 0;
        dir[Y] = 75;
        dir[Z] = 0;
        upV[X] = 0;
        upV[Y] = 1;
        upV[Z] = 0;
        calculateVectors();
    }
    void calculateVectors() {
     gazeV[X] = dir[X] - pos[X];
     gazeV[Z] = dir[Z] - pos[Z];
        float mag = sqrt(gazeV[X]*gazeV[X]+gazeV[Z]*gazeV[Z]);
        gazeV[X] /= mag;        // normalize gaze vector
        gazeV[Z] /= mag;
        rightV[X] = gazeV[Z];   // rightV = gazeV x upV
        rightV[Z] = -gazeV[X];
    }
};

/*
class ProcGenCam
{
public:
	ProcGenCam(float camX = 0.0f, float camY = 0.0f, float camZ = 5.0f,
			   float centerX = 0.0f, float centerY = 0.0f, float centerZ = -5.0f,
			   float eyeX = 0.0f, float eyeY = 1.0f, float eyeZ = 0.0f);
	~ProcGenCam(void);

	glm::vec3 getPosition(void);
	glm::vec3 getDirection(void);
	glm::vec3 getOrientation(void);

	void moveForward();
	void moveBackward();
	void strafeLeft();
	void strafeRight();

	void setDirection(float, float, bool);

private:
	// Position
	float m_camX,
		  m_camY,
		  m_camZ;
	// Direction
	float m_yaw,
		  m_pitch,
		  m_roll;
	// Orientation
	float m_eyeX,
		  m_eyeY,
		  m_eyeZ;

	// Mouse coordinates
	float m_previousCenterX,
		  m_previousCenterY;
};

*/