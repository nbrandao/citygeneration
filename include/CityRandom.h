#ifndef __CITYRANDOM__
#define __CITYRANDOM__

#include <random>
#include <chrono>

class CityRandom
{
	public:
		static CityRandom& get()
		{
			static CityRandom instance;
			return instance;
		}

		static int getRandom(int min, int max);

		static float getRandomFloat(float min, float max);

		private:
			CityRandom();
			CityRandom(const CityRandom&);
			CityRandom& operator=(const CityRandom&);

			static std::mt19937 Generator;
};

#endif