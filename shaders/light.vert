#version 420

layout (std140) uniform Matrices {
	mat4 pvm;
	mat4 viewModel;
	mat4 view;
	mat3 normal;
} Matrix;

// camera space
layout (std140) uniform Lights {
	vec3 dir;
} Light;

//Local Space
in vec4 position;
in vec3 normal;

out Data {
	vec3 normal;
	vec3 eye;
	vec3 lightDir;
} DataOut;

void main() {
	DataOut.normal = normalize(Matrix.normal * normal);
	DataOut.lightDir = Light.dir;
	DataOut.eye = - vec3(Matrix.viewModel * position);

	gl_Position = Matrix.pvm * position;
}