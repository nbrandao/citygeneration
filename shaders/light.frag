#version 420

layout (std140) uniform Materials {
	vec4 diffuse;
	vec4 ambient;
	vec4 specular;
	vec4 emissive;
	float shininess;
	int textCount;
} Material;

in Data {
	vec3 normal;
	vec3 eye;
	vec3 lightDir;
} DataIn;

out vec4 outputF;

void main() {
	vec4 spec = vec4(0.0);

	vec3 n = normalize(DataIn.normal);
	vec3 l = normalize(DataIn.lightDir);
	vec3 e = normalize(DataIn.eye);

	float intensity = max(dot(n,l), 0.0);

	if (intensity > 0.0){
		vec3 h = normalize(l + e);
		float intSpec = max(dot(h,n), 0.0);
		spec = Material.specular * pow(intSpec, Material.shininess);
	}

	outputF = max(intensity * Material.diffuse + spec, 0.25 * Material.diffuse);
}