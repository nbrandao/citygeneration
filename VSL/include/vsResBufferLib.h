#ifndef __VSResBufferLib__
#define __VSResBufferLib__

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include "vsResourceLib.h"

class VSResBufferLib : public VSResourceLib {
public:

	VSResBufferLib();

	virtual bool load(std::string filename);

	bool loadBuffers(float positions[], int numPosition, float normals[], int numNormal);

	virtual void render();

	void renderBuffers();

	void setColor(VSResourceLib::MaterialSemantics m, float *values);

	void setColor(unsigned int mesh, VSResourceLib::MaterialSemantics m, float *values);

	bool loaded();

	struct MyMesh
	{
		GLuint vao;
		GLuint uniformBlockIndex;
		int numPosition;
		int numNormal;
		unsigned int type;
		struct Material mat;
		float *positions;
		float *normals;
		bool loaded;
	};

	struct MyMesh *mMesh;
};
#endif
