#include "vsResBufferLib.h"

VSResBufferLib::VSResBufferLib()
{
	mMesh = new MyMesh();
	mMesh->loaded = false;
}

bool VSResBufferLib::loadBuffers(float positions[], int numPosition, float normals[], int numNormal)
{
	GLuint buffer;

	// Generate VAO for mesh
	glGenVertexArrays(1, &(mMesh->vao));
	glBindVertexArray(mMesh->vao);

	// Buffer for Positions
	int sizePositions = sizeof(float) * 4 * numPosition;
	mMesh->positions = (float *) malloc(sizePositions);
	memcpy(mMesh->positions, positions, sizePositions);

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizePositions, positions, GL_STATIC_DRAW);
	glEnableVertexAttribArray(VSShaderLib::VERTEX_COORD_ATTRIB);
	glVertexAttribPointer(VSShaderLib::VERTEX_COORD_ATTRIB,
		4, GL_FLOAT, 0, 0, 0);

	if (numNormal > 0) {
		int sizeNormals = sizeof(float) * 4 * numNormal;
		mMesh->normals = (float *) malloc(sizeNormals);
		memcpy(mMesh->normals, normals, sizeNormals);

		glGenBuffers(1, &buffer);
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeNormals, normals, GL_STATIC_DRAW);
		glEnableVertexAttribArray(VSShaderLib::NORMAL_ATTRIB);
		glVertexAttribPointer(VSShaderLib::NORMAL_ATTRIB, 
			4, GL_FLOAT, 0, 0, 0);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	mMesh->numPosition = numPosition;
	mMesh->numNormal = numNormal;
	mMesh->type = GL_TRIANGLES;

	mMesh->loaded = true;

	return true;
}

void VSResBufferLib::renderBuffers()
{

	mVSML->pushMatrix(VSMathLib::MODEL);

	// send matrices to shaders
	mVSML->matricesToGL();

	// set materials
	setMaterial(mMesh->mat);

	// bind VAO
	glBindVertexArray(mMesh->vao);
	glDrawArrays(mMesh->type, 0, mMesh->numPosition);

	mVSML->popMatrix(VSMathLib::MODEL);
	glBindVertexArray(0);
}


void VSResBufferLib::setColor(VSResourceLib::MaterialSemantics m, float *values) {

	if (m == TEX_COUNT)
		return;

	switch(m) {
	case SHININESS:
		mMesh->mat.shininess = *values;
		break;
	case DIFFUSE:
		memcpy(mMesh->mat.diffuse, values, sizeof(float)*4);
		break;
	case AMBIENT:
		memcpy(mMesh->mat.ambient, values, sizeof(float)*4);
		break;
	case SPECULAR:
		memcpy(mMesh->mat.specular, values, sizeof(float)*4);
		break;
	case EMISSIVE:
		memcpy(mMesh->mat.emissive, values, sizeof(float)*4);
		break;
	}
}


void VSResBufferLib::setColor(unsigned int mesh, VSResourceLib::MaterialSemantics m, float *values) {
}


bool VSResBufferLib::load(std::string filename){
	return false;
}

void VSResBufferLib::render(){
}

bool VSResBufferLib::loaded(){
	return mMesh->loaded;
}
