#include "Terreno.h"

void loadTerreno(VSResBufferLib *terreno, float width) {
	if(terreno->loaded())
		return;

	float diff_c[4] = {0.8f, 0.8f, 0.8f, 1.0f};

	int size = 6;
	width = width/2;

	GLfloat pos[] = {
		-width,0.0f,width,1.0f,
		width,0.0f,width,1.0f,
		-width,0.0f,-width,1.0f,

		-width,0.0f,-width,1.0f,
		width,0.0f,width,1.0f,
		width,0.0f,-width,1.0f,
	};

	float normal[] = {
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f
	};

	bool suc = terreno->loadBuffers(pos, size, normal, size);
	if(suc){
		terreno->setMaterialBlockName("Materials");
		terreno->setColor(VSResourceLib::DIFFUSE, diff_c);
	}
}