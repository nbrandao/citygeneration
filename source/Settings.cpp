#include "Settings.h"

Settings::Settings(){
	///////////////////////////////////////////////
	// Default Settings
	///////////////////////////////////////////////

	// Terreno Settings
	configs["terreno_size"] = 400;

	// Streets Settings
	configs["main_street_width"] = 4;
	configs["main_street_length_min"] = 15;
	configs["main_street_length_max"] = 30;
	configs["avenue_length_min"] = 10;
	configs["avenue_length_max"] = 20;
	configs["avenue_width"] = 2;
	configs["minimum_parallel_size"] = 5;
}

float Settings::getValue(std::string key){
	return configs[key];
}