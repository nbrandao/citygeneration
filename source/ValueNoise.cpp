#include "ValueNoise.h"

// 1D _____
ValueNoise1D::ValueNoise1D(uint16_t width,
						   uint16_t seed,
						   bool isSigned,
						   float lacunarity,
						   float gain,
						   uint16_t octaves,
						   Interpolation::InterpolationType iType) : WhiteNoise1D(M_RANDOM_SIZE,
																				  seed,
																				  isSigned)
{
	srand(seed);

	uint16_t m_iterations = -1,
			 m_power = 0;

	do m_iterations += 1;
	while ((m_power = (uint16_t)pow(2, m_iterations)) < width);

	this->m_heightMap = new vector<float>(m_power);
	for (uint16_t c = 0; c < this->m_heightMap->size(); c++)
		(*this->m_heightMap)(c) = 0.0f;

	this->m_isSigned = isSigned;
	this->m_maxVal = FLT_MIN;
	this->m_minVal = FLT_MAX;

	for (uint16_t i = 0; i < this->m_heightMap->size(); i++)
	{
		float m_total = 0.0f,
			  m_frequency = 1.0f / (float)this->m_heightMap->size(),
			  m_amplitude = gain;

		for (uint16_t o = 0; o < octaves; o++)
		{
			m_total += this->evaluate(iType, i * m_frequency) * m_amplitude;
			m_frequency *= lacunarity;
			m_amplitude *= gain;
		}
		(*this->m_heightMap)(i) = m_total;

		this->m_maxVal = glm::max((*this->m_heightMap)(i), this->m_maxVal);
		this->m_minVal = glm::min((*this->m_heightMap)(i), this->m_minVal);
	}
}

ValueNoise1D::~ValueNoise1D(void) {}

float ValueNoise1D::evaluate(Interpolation::InterpolationType iType, float x)
{
	float result = 0.0f;

	int x_int = (int)x;

	if (iType == Interpolation::Linear)
	{
		result = Interpolation::getInstance()->linearInterpolate(this->getValue(x_int),
																 this->getValue(x_int + 1),
																 x - (float)x_int);
	}

	if (iType == Interpolation::Cosine)
	{
		result = Interpolation::getInstance()->cosineInterpolate(this->getValue(x_int),
																 this->getValue(x_int + 1),
																 x - (float)x_int);
	}

	if (iType == Interpolation::Cubic)
	{
		result = Interpolation::getInstance()->cubicInterpolate(this->getValue(x_int - 1),
																this->getValue(x_int),
																this->getValue(x_int + 1),
																this->getValue(x_int + 2),
																x - (float)x_int);
	}

	if (iType == Interpolation::SmoothStep)
	{
		result = Interpolation::getInstance()->smoothStep(this->getValue(x_int),
														  this->getValue(x_int + 1),
														  x - (float)x_int);
	}

	return result;
}

GeneratorStatus ValueNoise1D::makeNoise(vector<float> *heightMap)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (this->m_heightMap->size() < heightMap->size())
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	{
		uint16_t start_i = (uint16_t)((this->m_heightMap->size() - heightMap->size()) / 2.0f);

		for (uint16_t i = 0; i < heightMap->size(); i++)
			(*heightMap)(i) = (((*this->m_heightMap)(i + start_i) - this->m_minVal) / (float)(this->m_maxVal - this->m_minVal)) - (this->m_isSigned?0.5f:0.0f);
	}

	return m_status;
}

// 2D _____
ValueNoise2D::ValueNoise2D(uint16_t width,
						   uint16_t height,
						   uint16_t seed,
						   bool isSigned,
						   float lacunarity,
						   float gain,
						   uint16_t octaves,
						   Interpolation::InterpolationType iType) : WhiteNoise2D(M_RANDOM_SIZE,
																				  M_RANDOM_SIZE,
																				  seed,
																				  isSigned)
{
	srand(seed);

	uint16_t m_iterations = -1,
			 m_power = 0,
			 m_minSize = glm::max(width, height);

	do m_iterations += 1;
	while ((m_power = (uint16_t)pow(2, m_iterations)) < m_minSize);

	this->m_heightMap = new matrix<float>(m_power, m_power);
	for (uint16_t c = 0; c < this->m_heightMap->size1(); c++)
		(*this->m_heightMap)(c, c) = 0.0f;

	this->m_isSigned = isSigned;
	this->m_maxVal = FLT_MIN;
	this->m_minVal = FLT_MAX;

	for (uint16_t i = 0; i < this->m_heightMap->size1(); i++)
		for (uint16_t j = 0; j < this->m_heightMap->size2(); j++)
		{
			float m_total = 0.0f,
				  m_frequency = 1.0f / (float)((this->m_heightMap->size1() + this->m_heightMap->size2()) / 2.0f),
				  m_amplitude = gain;

			for (int o = 0; o < octaves; o++)
			{
				m_total += this->evaluate(iType, i * m_frequency, j * m_frequency) * m_amplitude;
				m_frequency *= lacunarity;
				m_amplitude *= gain;
			}
			(*this->m_heightMap)(i, j) = m_total;

			this->m_maxVal = glm::max((*this->m_heightMap)(i, j), this->m_maxVal);
			this->m_minVal = glm::min((*this->m_heightMap)(i, j), this->m_minVal);
		}
}

ValueNoise2D::~ValueNoise2D(void) {}

float ValueNoise2D::evaluate(Interpolation::InterpolationType iType, float x, float y)
{
	float result = 0.0f;

	int x_int = (int)x,
		y_int = (int)y;

	if (iType == Interpolation::Linear)
	{
		float v1 = this->getValue(x_int, y_int),				// (0,0)
			  v2 = this->getValue(x_int + 1, y_int),			// (1,0)
			  v3 = this->getValue(x_int, y_int + 1),			// (0,1)
			  v4 = this->getValue(x_int + 1, y_int + 1);		// (1,1)

		float t1 = Interpolation::getInstance()->linearInterpolate(v1, v2, x - (float)x_int),	// Interpolate between (0,0) - (1,0) @ x axis
			  t2 = Interpolation::getInstance()->linearInterpolate(v3, v4, x - (float)x_int);	// Interpolate between (0,1) - (1,1) @ x axis

		result = Interpolation::getInstance()->linearInterpolate(t1, t2, y - (float)y_int);		// Interpolate values @ Y axis
	}

	if (iType == Interpolation::Cosine)
	{
		float v1 = this->getValue(x_int, y_int),				// (0,0)
			  v2 = this->getValue(x_int + 1, y_int),			// (1,0)
			  v3 = this->getValue(x_int, y_int + 1),			// (0,1)
			  v4 = this->getValue(x_int + 1, y_int + 1);		// (1,1)

		float t1 = Interpolation::getInstance()->cosineInterpolate(v1, v2, x - (float)x_int),	// Interpolate between (0,0) - (1,0) @ x axis
			  t2 = Interpolation::getInstance()->cosineInterpolate(v3, v4, x - (float)x_int);	// Interpolate between (0,1) - (1,1) @ x axis

		result = Interpolation::getInstance()->cosineInterpolate(t1, t2, y - (float)y_int);		// Interpolate values @ Y axis
	}

	if (iType == Interpolation::Cubic)
	{
			  // Interpolate between (-1,-1) - (2,-1) @ X axis
		float t1 = Interpolation::getInstance()->cubicInterpolate(this->getValue(x_int - 1, y_int - 1),		// (-1,-1)
																  this->getValue(x_int, y_int - 1),			// (0,-1)
																  this->getValue(x_int + 1, y_int - 1),		// (1,-1)
																  this->getValue(x_int + 2, y_int - 1),		// (2,-1)
																  x - (float)x_int),
			  // Interpolate between (-1,0) - (2,0) @ X axis
			  t2 = Interpolation::getInstance()->cubicInterpolate(this->getValue(x_int - 1, y_int),			// (-1,0)
																  this->getValue(x_int, y_int),				// (0,0)
																  this->getValue(x_int + 1, y_int),			// (1,0)
																  this->getValue(x_int + 2, y_int),			// (2,0)
																  x - (float)x_int),
			  // Interpolate between (-1,1) - (2,1) @ X axis
			  t3 = Interpolation::getInstance()->cubicInterpolate(this->getValue(x_int - 1, y_int + 1),		// (-1,1)
																  this->getValue(x_int, y_int + 1),			// (0,1)
																  this->getValue(x_int + 1, y_int + 1),		// (1,1)
																  this->getValue(x_int + 2, y_int + 1),		// (2,1)
																  x - (float)x_int),
			  // Interpolate between (-1,2) - (2,2) @ X axis
			  t4 = Interpolation::getInstance()->cubicInterpolate(this->getValue(x_int - 1, y_int + 2),		// (-1,2)
																  this->getValue(x_int, y_int + 2),			// (0,2)
																  this->getValue(x_int + 1, y_int + 2),		// (1,2)
																  this->getValue(x_int + 2, y_int + 2),		// (2,2)
																  x - (float)x_int);
			
		result = Interpolation::getInstance()->cubicInterpolate(t1, t2, t3, t4, y - (float)y_int); 		// Interpolate values @ Y axis
	}

	if (iType == Interpolation::SmoothStep)
	{
		float v1 = this->getValue(x_int, y_int),				// (0,0)
			  v2 = this->getValue(x_int + 1, y_int),			// (1,0)
			  v3 = this->getValue(x_int, y_int + 1),			// (0,1)
			  v4 = this->getValue(x_int + 1, y_int + 1);		// (1,1)

		float t1 = Interpolation::getInstance()->smoothStep(v1, v2, x - (float)x_int),	// Interpolate between (0,0) - (1,0) @ x axis
			  t2 = Interpolation::getInstance()->smoothStep(v3, v4, x - (float)x_int);	// Interpolate between (0,1) - (1,1) @ x axis

		result = Interpolation::getInstance()->smoothStep(t1, t2, y - (float)y_int);		// Interpolate values @ Y axis
	}

	return result;
}

GeneratorStatus ValueNoise2D::makeNoise(matrix<float> *heightMap)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if ((this->m_heightMap->size1() < heightMap->size1()) ||
		(this->m_heightMap->size2() < heightMap->size2()))
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	{
		uint16_t start_i = (uint16_t)((this->m_heightMap->size1() - heightMap->size1()) / 2.0f),
				 start_j = (uint16_t)((this->m_heightMap->size2() - heightMap->size2()) / 2.0f);

		for (uint16_t i = 0; i < heightMap->size1(); i++)
			for (uint16_t j = 0; j < heightMap->size2(); j++)
				(*heightMap)(i, j) = (((*this->m_heightMap)(i + start_i, j + start_j) - this->m_minVal) / (float)(this->m_maxVal - this->m_minVal)) - (this->m_isSigned?0.5f:0.0f);
	}

	return m_status;
}
