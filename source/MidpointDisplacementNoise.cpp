#include "MidpointDisplacementNoise.h"

// 1D _____
MidpointDisplacementNoise1D::MidpointDisplacementNoise1D(uint16_t width,
														 uint16_t seed,
														 bool isSigned,
														 float hardness)
{
	srand(seed);

	uint16_t m_iterations = -1,
			 m_power = 0;

	do m_iterations += 1;
	while ((m_power = (uint16_t)pow(2, m_iterations)) < width);
	this->m_heightMap = new vector<float>(m_power);
	for (uint16_t c = 0; c < this->m_heightMap->size(); c++)
		(*this->m_heightMap)(c) = 0.0f;

	this->m_isSigned = isSigned;
	this->m_maxVal = FLT_MIN;
	this->m_minVal = FLT_MAX;

	uint16_t it = 0;
	float h = glm::max(0.0f, glm::min(1.0f, hardness));

	while (it < m_iterations)
	{
		uint16_t m_step = (uint16_t)(m_power / (float)(pow(2,it) * 2.0f));
		for (uint16_t i = 0; i < (m_power - m_step); i += m_step)
		{
			uint16_t *m_previousI = &i,
					 m_currentI = *m_previousI + m_step - 1,
					 m_nextI = *m_previousI + (2 * m_step) - 1;

			(*this->m_heightMap)(m_currentI) = ((*this->m_heightMap)(*m_previousI) +
												(*this->m_heightMap)(m_nextI)) / 2.0f +
											   ((this->m_isSigned?(2.0f * ((float)rand() / (float)RAND_MAX)) - 1.0f:
																  ((float)rand() / (float)RAND_MAX)) * h);

			for (uint16_t t = 0; t < m_step; t++)
			{
				(*this->m_heightMap)(t + *m_previousI) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(*m_previousI),
																										 (*this->m_heightMap)(m_currentI),
																										 t / (float)m_step);
				(*this->m_heightMap)(t + m_currentI) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(m_currentI),
																										   (*this->m_heightMap)(m_nextI),
																										   t / (float)m_step);
			}
			this->m_maxVal = glm::max((*this->m_heightMap)(m_currentI), this->m_maxVal);
			this->m_minVal = glm::min((*this->m_heightMap)(m_currentI), this->m_minVal);
		}
		it += 1;
		h *= h;
	}
}

MidpointDisplacementNoise1D::~MidpointDisplacementNoise1D(void)
{
	delete this->m_heightMap;
}

GeneratorStatus MidpointDisplacementNoise1D::makeNoise(vector<float> *heightMap)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (this->m_heightMap->size() < heightMap->size())
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	{
		uint16_t start_i = (uint16_t)((this->m_heightMap->size() - heightMap->size()) / 2.0f);

		for (uint16_t i = 0; i < heightMap->size(); i++)
			(*heightMap)(i) = (((*this->m_heightMap)(i + start_i) - this->m_minVal) / (float)(this->m_maxVal - this->m_minVal)) - (this->m_isSigned?0.5f:0.0f);
	}

	return m_status;
}

// 2D _____
MidpointDisplacementNoise2D::MidpointDisplacementNoise2D(uint16_t width,
														 uint16_t height,
														 uint16_t seed,
														 bool isSigned,
														 float hardness)
{
	srand(seed);

	uint16_t m_iterations = -1,
			 m_power = 0,
			 m_minSize = glm::max(width, height);

	do m_iterations += 1;
	while ((m_power = (uint16_t)pow(2, m_iterations)) < m_minSize);

	this->m_heightMap = new matrix<float>(m_power, m_power);
	for (uint16_t c = 0; c < this->m_heightMap->size1(); c++)
		(*this->m_heightMap)(c, c) = 0.0f;

	this->m_isSigned = isSigned;
	this->m_maxVal = FLT_MIN;
	this->m_minVal = FLT_MAX;

	uint16_t it = 0;
	float h = hardness;
	
	while (it < m_iterations)
	{
		uint16_t m_step = (uint16_t)(m_power / (float)(powf(2.0f, (float)it) * 2.0f));
		for (uint16_t i = 0; i < (m_power - m_step); i += m_step)
			for (uint16_t j = 0; j < (m_power - m_step); j += m_step)
			{
				uint16_t *m_previousI = &i,
						 *m_previousJ = &j,
						 m_currentI = *m_previousI + m_step - 1,
						 m_currentJ = *m_previousJ + m_step - 1,
						 m_nextI = *m_previousI + (2 * m_step) - 1,
						 m_nextJ = *m_previousJ + (2 * m_step) - 1;

				(*this->m_heightMap)(m_currentI, m_currentJ) = (((*this->m_heightMap)(*m_previousI, *m_previousJ) +
																 (*this->m_heightMap)(*m_previousI, m_nextJ) +
																 (*this->m_heightMap)(m_nextI, *m_previousJ) +
																 (*this->m_heightMap)(m_nextI, m_nextJ)) / 4.0f) +
															    ((this->m_isSigned?(2.0f * ((float)rand() / (float)RAND_MAX)) - 1.0f:
																				   ((float)rand()/(float)RAND_MAX)) * h);

				// Average corner middle values
				(*this->m_heightMap)(*m_previousI, m_currentJ) = ((*this->m_heightMap)(*m_previousI, *m_previousJ) +
																  (*this->m_heightMap)(*m_previousI, m_nextJ)) / 2.0f;

				(*this->m_heightMap)(m_nextI, m_currentJ) = ((*this->m_heightMap)(m_nextI, *m_previousJ) +
															 (*this->m_heightMap)(m_nextI, m_nextJ)) / 2.0f;

				(*this->m_heightMap)(m_currentI, *m_previousJ) = ((*this->m_heightMap)(*m_previousI, *m_previousJ) +
																  (*this->m_heightMap)(m_nextI, *m_previousJ)) / 2.0f;

				(*this->m_heightMap)(m_currentI, m_nextJ) = ((*this->m_heightMap)(*m_previousI, m_nextJ) +
															 (*this->m_heightMap)(m_nextI, m_nextJ)) / 2.0f;


				for (uint16_t t = 0; t < m_step; t++)
				{
					// Horizontal LEFT _____________
					// Interpolate (0,0) - (0.5,0)
					(*this->m_heightMap)(*m_previousI + t, *m_previousJ) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(*m_previousI, *m_previousJ),
																														   (*this->m_heightMap)(m_currentI, *m_previousJ),
																														   t / (float)m_step);
					// Interpolate (0,0.5) - (0.5,0.5)
					(*this->m_heightMap)(*m_previousI + t, m_currentJ) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(*m_previousI, m_currentJ),
																														 (*this->m_heightMap)(m_currentI, m_currentJ),
																														 t / (float)m_step);
					// Interpolate (0,1) - (0.5,1)
					(*this->m_heightMap)(*m_previousI + t, m_nextJ) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(*m_previousI, m_nextJ),
																													  (*this->m_heightMap)(m_currentI, m_nextJ),
																													  t / (float)m_step);

					// Horizontal RIGHT ____________
					// Interpolate (0.5,0) - (1,0)
					(*this->m_heightMap)(m_currentI + t, *m_previousJ) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(m_currentI, *m_previousJ),
																														 (*this->m_heightMap)(m_nextI, *m_previousJ),
																														 t / (float)m_step);
					// Interpolate (0.5,0.5) - (1,0.5)
					(*this->m_heightMap)(m_currentI + t, m_currentJ) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(m_currentI, m_currentJ),
																													   (*this->m_heightMap)(m_nextI, m_currentJ),
																													   t / (float)m_step);
					// Interpolate (0.5,1) - (1,1)
					(*this->m_heightMap)(m_currentI + t, m_nextJ) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(m_currentI, m_nextJ),
																													(*this->m_heightMap)(m_nextI, m_nextJ),
																													t / (float)m_step);

					for (uint16_t u = 0; u < m_step; u++)
					{
						// Vertical LEFT _______________
						// Interpolate (0,0) - (0.5,0.5)
						(*this->m_heightMap)(*m_previousI + t, *m_previousJ + u) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(*m_previousI + t, *m_previousJ),
																																   (*this->m_heightMap)(*m_previousI + t, m_currentJ),
																																   u / (float)m_step);
						// Interpolate (0,0.5) - (0.5,1)
						(*this->m_heightMap)(*m_previousI + t, m_currentJ + u) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(*m_previousI + t, m_currentJ),
																																 (*this->m_heightMap)(*m_previousI + t, m_nextJ),
																																 u / (float)m_step);

						// Vertical RIGHT ______________
						// Interpolate (0.5,0) - (1,0.5)
						(*this->m_heightMap)(m_currentI + t, *m_previousJ + u) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(m_currentI + t, *m_previousJ),
																																 (*this->m_heightMap)(m_currentI + t, m_currentJ),
																																 u / (float)m_step);
						// Interpolate (0.5,0.5) - (1,1)
						(*this->m_heightMap)(m_currentI + t, m_currentJ + u) = Interpolation::getInstance()->linearInterpolate((*this->m_heightMap)(m_currentI + t, m_currentJ),
																															   (*this->m_heightMap)(m_currentI + t, m_nextJ),
																															   u / (float)m_step);

					}
				}
				this->m_maxVal = glm::max((*this->m_heightMap)(m_currentI, m_currentJ), this->m_maxVal);
				this->m_minVal = glm::min((*this->m_heightMap)(m_currentI, m_currentJ), this->m_minVal);
			}
		it += 1;
		h *= h;
	}
}

MidpointDisplacementNoise2D::~MidpointDisplacementNoise2D(void)
{
	delete this->m_heightMap;
}

GeneratorStatus MidpointDisplacementNoise2D::makeNoise(matrix<float> *heightMap)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;
	
	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if ((this->m_heightMap->size1() < heightMap->size1()) ||
		(this->m_heightMap->size2() < heightMap->size2()))
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	{
		uint16_t start_i = (uint16_t)((this->m_heightMap->size1() - heightMap->size1()) / 2.0f),
				 start_j = (uint16_t)((this->m_heightMap->size2() - heightMap->size2()) / 2.0f);

		for (uint16_t i = 0; i < heightMap->size1(); i++)
			for (uint16_t j = 0; j < heightMap->size2(); j++)
				(*heightMap)(i, j) = (((*this->m_heightMap)(i + start_i, j + start_j) - this->m_minVal) / (float)(this->m_maxVal - this->m_minVal)) - (this->m_isSigned?0.5f:0.0f);
	}

	return m_status;
}
