#include "Common.h"

float vec2dotProduct(glm::vec2 v1, glm::vec2 v2)
{
	return (v1.x * v2.x) + (v1.y * v2.y);
}

glm::vec3 vec3crossProduct(glm::vec3 n1, glm::vec3 n2)
{
	return glm::vec3(n1.y * n2.z - n1.z * n2.y,
					 n1.z * n2.x - n1.x * n2.z,
					 n1.x * n2.y - n1.y * n2.x);
}

glm::vec3 vec3normalize(glm::vec3 n)
{
	float m_length = sqrtf(powf(n.x, 2.0) + powf(n.y, 2.0) + powf(n.z, 2.0));
	return glm::vec3(n.x / m_length, n.y / m_length, n.z / m_length);
}