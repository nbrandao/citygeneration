#include "Erosion.h"

Erosion *Erosion::getInstance()
{
	if (!instance)
		instance = new Erosion();
	return instance;
}
Erosion *Erosion::instance = 0;
Erosion::Erosion(void) {}
Erosion::~Erosion(void) {}

GeneratorStatus Erosion::thermalErode(matrix<float> *heightMap,
									  uint16_t iterations,
									  float talus)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (!heightMap->size1() || !heightMap->size2())
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	if ((!iterations) || (talus <= 0.0f))
		m_status = GeneratorStatus::GEN_BAD_VALUE;
	else
	{
		uint16_t it = 0;

		while (it < iterations)
		{
			for (uint16_t i = 1; i < heightMap->size1() - 1; i++)
				for (uint16_t j = 1; j < heightMap->size2() - 1; j++)
				{
					float m_height = (*heightMap)(i, j),
						  m_max_difference = -FLT_MAX;
					int m_max_x = 0,
						m_max_y = 0;

					//// Moore neighborhood
					//for (int x = -1; x < 2; x++)
					//	for (int y = -1; y < 2; y++)
					//	{
					//		float m_curr_difference = m_height - (*heightMap)(i + x, j + y);
					//		if (m_curr_difference > m_max_difference)
					//		{
					//			m_max_difference = m_curr_difference;
					//			m_max_x = x;
					//			m_max_y = y;
					//		}
					//		if (m_max_difference > talus)
					//		{
					//			float m_new_height = m_height - m_max_difference / 2.0f;
					//			(*heightMap)(i, j) = m_new_height;
					//			(*heightMap)(i + m_max_x, j + m_max_y) = m_new_height;
					//		}
					//	}

					// Von Neumann neighborhood
					for (int x = -1; x < 2; x++)
						for (int y = -1; y < 2; y++)
							if (abs(x - y) == 1)
							{
								float m_curr_difference = m_height - (*heightMap)(i + x, j + y);
								if (m_curr_difference > m_max_difference)
								{
									m_max_difference = m_curr_difference;
									m_max_x = x;
									m_max_y = y;
								}
								if (m_max_difference > talus)
								{
									float m_new_height = m_height - m_max_difference / 2.0f;
									(*heightMap)(i, j) = m_new_height;
									(*heightMap)(i + m_max_x, j + m_max_y) = m_new_height;
								}
							}
				}
			it += 1;
		}
	}

	return m_status;
}

GeneratorStatus Erosion::improvedErode(matrix<float> *heightMap,
									   uint16_t iterations,
									   float talus)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (!heightMap->size1() || !heightMap->size2())
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	if ((!iterations) || (talus <= 0.0f))
		m_status = GeneratorStatus::GEN_BAD_VALUE;
	else
	{
		uint16_t it = 0;

		while (it < iterations)
		{
			for (uint16_t i = 1; i < heightMap->size1() - 1; i++)
				for (uint16_t j = 1; j < heightMap->size2() - 1; j++)
				{
					float m_height = (*heightMap)(i, j),
						  m_max_difference = -FLT_MAX;
					int m_max_x = 0,
						m_max_y = 0;

					//// Moore neighborhood
					//for (int x = -1; x < 2; x++)
					//	for (int y = -1; y < 2; y++)
					//	{
					//		float m_curr_difference = m_height - (*heightMap)(i + x, j + y);
					//		if (m_curr_difference > m_max_difference)
					//		{
					//			m_max_difference = m_curr_difference;
					//			m_max_x = x;
					//			m_max_y = y;
					//		}
					//		if (m_max_difference > talus)
					//		{
					//			float m_new_height = m_height - m_max_difference / 2.0f;
					//			(*heightMap)(i, j) = m_new_height;
					//			(*heightMap)(i + m_max_x, j + m_max_y) = m_new_height;
					//		}
					//	}

					// Von Neumann neighborhood
					for (int x = -1; x < 2; x++)
						for (int y = -1; y < 2; y++)
							if (abs(x - y) == 1)
							{
								float m_curr_difference = m_height - (*heightMap)(i + x, j + y);
								if (m_curr_difference > m_max_difference)
								{
									m_max_difference = m_curr_difference;
									m_max_x = x;
									m_max_y = y;
								}
								if ((m_max_difference > 0.0f) && (m_max_difference <= talus))
								{
									float m_new_height = m_height - m_max_difference / 2.0f;
									(*heightMap)(i, j) = m_new_height;
									(*heightMap)(i + m_max_x, j + m_max_y) = m_new_height;
								}
							}
				}
			it += 1;
		}
	}

	return m_status;
}


/*
void Erosion::hydraulicErode()
{
	;
}
*/
