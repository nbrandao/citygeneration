#include "WhiteNoise.h"

// 1D __________
WhiteNoise1D::WhiteNoise1D(uint16_t w, uint16_t seed, bool isSigned)
{
	srand(seed);
	this->m_randomVector = new vector<float>(w);
	for (uint16_t i = 0; i < this->m_randomVector->size(); i++)
		(*this->m_randomVector)(i) = isSigned?(2.0f * ((float)rand()/(float)RAND_MAX)) - 1.0f:((float)rand()/(float)RAND_MAX);
}

WhiteNoise1D::~WhiteNoise1D(void)
{
	delete this->m_randomVector;
}

vector<float> *WhiteNoise1D::getNoise(void)
{
	return this->m_randomVector;
}

float WhiteNoise1D::getValue(int x)
{
	uint16_t m_x = (x >= 0)?x:this->getWidth() - x;
	return (*this->m_randomVector)(m_x % this->getWidth());
}

uint16_t WhiteNoise1D::getWidth(void)
{
	return this->m_randomVector->size();
}

float WhiteNoise1D::evaluate(Interpolation::InterpolationType iType, float x)
{
	return 0.0f;
}

// 2D __________
WhiteNoise2D::WhiteNoise2D(uint16_t w, uint16_t h, uint16_t seed, bool isSigned)
{
	srand(seed);
	this->m_randomMatrix = new matrix<float>(w, h);
	for (uint16_t i = 0; i < this->m_randomMatrix->size1(); i++)
		for (uint16_t j = 0; j < this->m_randomMatrix->size2(); j++)
			(*this->m_randomMatrix)(i, j) = isSigned?(2 * ((float)rand()/(float)RAND_MAX)) - 1.0f:((float)rand()/(float)RAND_MAX);
}

WhiteNoise2D::~WhiteNoise2D(void)
{
	delete this->m_randomMatrix;
}

matrix<float> *WhiteNoise2D::getNoise(void)
{
	return this->m_randomMatrix;
}

float WhiteNoise2D::getValue(int x, int y)
{
	uint16_t m_x = (x >= 0)?x:this->getWidth() - x,
			 m_y = (y >= 0)?y:this->getWidth() - y;
	return (*this->m_randomMatrix)(m_x % this->getWidth(), m_y % this->getHeight());
}

uint16_t WhiteNoise2D::getWidth(void)
{
	return this->m_randomMatrix->size1();
}

uint16_t WhiteNoise2D::getHeight(void)
{
	return this->m_randomMatrix->size2();
}

float WhiteNoise2D::evaluate(Interpolation::InterpolationType iType, float x, float y)
{
	return 0.0f;
}