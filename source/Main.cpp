#include <math.h>
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include <stdio.h>

// include GLEW to access OpenGL 3.3 functions
#include <GL/glew.h>

// GLUT is the toolkit to interface with the OS
#include <GL/freeglut.h>

// Use Very Simple Libs
#include "vslibs.h"
#include "StreetGen.h"
#include "Render.h"

std::ostream *f;
VSMathLib *vsml;
VSResourceLib *vsrl;
VSShaderLib shader,shaderTess;
VSFontLib vsfl2, vsfl;
unsigned int aSentence, profileSentence;

// Query to track the number of primitives
// issued by the tesselation shaders
GLuint counterQ;
unsigned int primitiveCounter = 0;

// FPS Cam
const int left = 1; 
const int right = 2;
const int forward = 4; 
const int backward = 8; 
const int up = 16; 
const int down = 32;

int move = 0;

float angles_x = 0.0;
float angles_y = 0.0;
float lookat_x = 0.0;
float lookat_y = 0.0;
float lookat_z = 0.0;

float position_x = 0.0;
float position_y = 10.0;
float position_z = 20.0;

float mouse_x = 0;
float mouse_y = 0;

bool next;

// Frame counting and FPS computation
long myTime,timebase = 0,frame = 0;
char s[32];

// Light direction
float lightDir[4] = {1.0f, 1.0f, 1.0f, 0.0f};
float lightPos[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float spotDir[4]  = {-4.0f, -6.0f, -2.0f, 0.0f};

float diff_c[4] = {0.6f, 0.6f, 0.6f, 1.0f};

float shininess[1] = {100};

void renderScene(void) {

	{
		PROFILE("Frame");
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Set both matrices to the identity matrix
		vsml->loadIdentity(VSMathLib::VIEW);
		vsml->loadIdentity(VSMathLib::MODEL);

		// set camera
		vsml->lookAt(position_x, position_y, position_z, position_x + lookat_x, position_y + lookat_y, position_z + lookat_z, 0,1,0);

		float res[4];
		// Convert to camera Space
		vsml->multMatrixPoint(VSMathLib::VIEW, lightDir, res);
		// Normalize
		vsml->normalize(res);
		shader.setBlockUniform("Lights", "dir", res);
		shader.setBlockUniform("Lights", "pos", lightPos);

		{
			PROFILE_GL("Render models");

			// set the shader to render models
			glUseProgram(shader.getProgramIndex());
			// start counting primitives
			glBeginQuery(GL_PRIMITIVES_GENERATED, counterQ);

			render();

			// stop counting primitives
			glEndQuery(GL_PRIMITIVES_GENERATED);
		}

		// FPS computation and display
		frame++;
		myTime=glutGet(GLUT_ELAPSED_TIME);
		if (myTime - timebase > 1000) {
			sprintf(s,"FPS:%4.2f  Counter: %d", frame*1000.0/(myTime-timebase) , primitiveCounter);
			timebase = myTime;
			frame = 0;
			vsfl.prepareSentence(aSentence,s);
			glutSetWindowTitle(s);
		}

		// Display text info
		{
			PROFILE("Dump");
			// prepare sentence with profile info
			std::string s = VSProfileLib::DumpLevels();
			vsfl.prepareSentence(profileSentence, s);
			//set the shader for rendering the sentence
			glUseProgram(shader.getProgramIndex());
			// render sentences
			vsfl.renderSentence(10,10,aSentence);
			vsfl.renderSentence(10, 30, profileSentence);

		}
		//swap buffers
		{
			PROFILE("Swap");
			glutSwapBuffers();
		}
	} // end PROFILE("Frame")
	{
		PROFILE("Collect GL Queries Time");
		VSProfileLib::CollectQueryResults();
		glGetQueryObjectuiv(counterQ, GL_QUERY_RESULT, &primitiveCounter);
	}
}

void key_pressed(unsigned char key, int x, int y) {
	if(key == 'a')
		move |= left;     
	if(key == 'd')
		move |= right;
	if(key == 'w')
		move |= forward;     
	if(key == 's')
		move |= backward;     
	if(key == 'q')
		move |= up;     
	if(key == 'e')
		move |= down;
	if(key == 'n')
		next = true;
}

void key_released(unsigned char key, int x, int y) {
	if(key == 'a')
		move &= ~left;     
	if(key == 'd')
		move &= ~right;
	if(key == 'w')
		move &= ~forward;     
	if(key == 's')
		move &= ~backward;     
	if(key == 'q')
		move &= ~up;     
	if(key == 'e')
		move &= ~down;
}

void processMouseMotion(int x, int y) {
	static bool wrap = false;

	if(!wrap) {
		int dx = x - mouse_x;
		int dy = y - mouse_y;

    	// Do something with dx and dy here
		const float mousespeed = 0.001;

		angles_x -= dx * mousespeed;
		angles_y -= dy * mousespeed;

		if(angles_x < -M_PI)
			angles_x += M_PI * 2;
		else if(angles_x > M_PI)
			angles_x -= M_PI * 2;

		if(angles_y < -M_PI / 2)
			angles_y = -M_PI / 2;
		if(angles_y > M_PI / 2)
			angles_y = M_PI / 2;

		lookat_x = sinf(angles_x) * cosf(angles_y);
		lookat_y = sinf(angles_y);
		lookat_z = cosf(angles_x) * cosf(angles_y);

    	// move mouse pointer back to the center of the window
		wrap = true;
		glutWarpPointer(mouse_x, mouse_y);
	} else {
		wrap = false;
	}
}

void processMouse(int button, int state, int x, int y){
	if(button == GLUT_LEFT_BUTTON)
	{
		if(state == GLUT_DOWN){
			glutSetCursor(GLUT_CURSOR_NONE);
			mouse_x = x;
			mouse_y = y;
		}
		else
			glutSetCursor(GLUT_CURSOR_INHERIT);
	} else if(button == GLUT_RIGHT_BUTTON)
		glutFullScreen();
}

void idle() {
	static int pt = 0;
	const float movespeed = 80;

  	// Calculate time since last call to idle()
	int t = glutGet(GLUT_ELAPSED_TIME);
	float dt = (t - pt) * 1.0e-3;
	pt = t;

  	// Calculate movement vectors
	float forward_dir_x = sinf(angles_x);
	float forward_dir_z = cosf(angles_x);

	float right_dir_x = -forward_dir_z;
	float right_dir_z = forward_dir_x;

  	// Update camera position
	if(move & left){
		position_x -= right_dir_x * movespeed * dt;
		position_z -= right_dir_z * movespeed * dt;
	}
	if(move & right){
		position_x += right_dir_x * movespeed * dt;
		position_z += right_dir_z * movespeed * dt;
	}
	if(move & forward){
		position_x += forward_dir_x * movespeed * dt;
		position_z += forward_dir_z * movespeed * dt;
	}
	if(move & backward){
		position_x -= forward_dir_x * movespeed * dt;
		position_z -= forward_dir_z * movespeed * dt;
	}
	if(move & up){
		position_y += movespeed * dt;
	}
	if(move & down){
		position_y -= movespeed * dt;
	}

  	// Redraw the scene
	glutPostRedisplay();
}

// Reshape Callback Function
void changeSize(int w, int h) {

	float ratio;
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
		h = 1;

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	ratio = (1.0f * w) / h;
	vsml->loadIdentity(VSMathLib::PROJECTION);
	vsml->perspective(53.13f, ratio, 0.1f, 10000.0f);
}

// Shader Stuff
GLuint setupShaders() {

	// Shader for fonts and models
	shader.init();

	shader.loadShader(VSShaderLib::VERTEX_SHADER, "../shaders/light.vert");
	shader.loadShader(VSShaderLib::FRAGMENT_SHADER, "../shaders/light.frag");

	// set semantics for the shader variables
	shader.setProgramOutput(0,"outputF");
	shader.setVertexAttribName(VSShaderLib::VERTEX_COORD_ATTRIB, "position");
	shader.setVertexAttribName(VSShaderLib::NORMAL_ATTRIB, "normal");

	shader.prepareProgram();

	VSGLInfoLib::getProgramInfo(shader.getProgramIndex());
	printf("%s\n", shader.getAllInfoLogs().c_str());
	// set sampler uniform
	shader.setUniform("texUnit", 0);

	return(1);
}

// OpenGL setup
int init(){
	// some GL settings
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_MULTISAMPLE);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	// generate a query to count primitives
	glGenQueries(1,&counterQ);

	return true;
}

void initVSL() {

	// set the material's block name
	//VSResourceLib::setMaterialBlockName("Materials");

	// Init VSML
	vsml = VSMathLib::getInstance();
	vsml->setUniformBlockName("Matrices");
	vsml->setUniformName(VSMathLib::PROJ_VIEW_MODEL, "pvm");
	vsml->setUniformName(VSMathLib::VIEW_MODEL, "viewModel");
	vsml->setUniformName(VSMathLib::VIEW, "view");
	vsml->setUniformName(VSMathLib::NORMAL, "normal");

	// Init VSFL Fonts
	vsfl.load("../res/fonts/couriernew10");
	vsfl.setFixedFont(true);
	vsfl.setColor(1.0f, 0.5f, 0.25f, 1.0f);
	aSentence = vsfl.genSentence();
	profileSentence = vsfl.genSentence();
}

// Main function
int main(int argc, char **argv) {
	//  GLUT initialization
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA|GLUT_MULTISAMPLE);

	// Set context
	glutInitContextVersion (4, 2);
	glutInitContextProfile (GLUT_CORE_PROFILE );

	glutInitWindowPosition(100,100);
	glutInitWindowSize(1366,768);
	glutCreateWindow("Lighthouse3D - VSL Demo");


	//  Callback Registration
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);

	//	Mouse and Keyboard Callbacks
	glutKeyboardFunc(key_pressed);
	glutKeyboardUpFunc(key_released);
	glutMotionFunc(processMouseMotion);
	glutMouseFunc(processMouse);
	glutIdleFunc(idle);

	//	return from main loop
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	//	Init GLEW
	glewExperimental = GL_TRUE;
	glewInit();
	if (!glewIsSupported("GL_VERSION_3_3")) {
		printf("OpenGL 3.3 not supported\n");
		exit(1);
	}

	// this function will display generic OpenGL information
	VSGLInfoLib::getGeneralInfo();

	setupShaders();

	// init OpenGL and load model
	if (!init()) {
		printf("Could not Load the Model\n");
		exit(1);
	}

	initVSL();

	preloadBuffers();
	srand(time(NULL));

	//  GLUT main loop
	glutMainLoop();

	return(1);

}

