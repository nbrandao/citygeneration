#include "Terrain.h"

Terrain::Terrain()
{
	this->m_heightMap = 0;
	this->m_regionMask = 0;
	this->m_positions = 0;
	this->m_normals = 0;

	this->m_isSigned = false;
	this->m_terrainSet = false;
	this->m_citySet = false;
	this->m_riverSet = false;
	this->m_forestSet = false;
}

Terrain::~Terrain(void)
{
	if (this->m_heightMap)
		delete this->m_heightMap;
	if (this->m_regionMask)
		delete this->m_regionMask;
	if (this->m_positions)
		delete this->m_positions;
	if (this->m_normals)
		delete this->m_normals;
}

GeneratorStatus Terrain::init(uint16_t seed,
							  bool isSigned,
							  uint16_t xw,
							  uint16_t zw,
							  float xz_grid,
							  float y_height)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_ALREADY_ALLOCATED;
	else
	if ((!xw) || (!zw))
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	if ((xz_grid <= 0.0f) || (y_height <= 0.0f))
		m_status = GeneratorStatus::GEN_BAD_VALUE;
	else
	{
		this->m_heightMap = new matrix<float>(xw, zw);
		this->m_regionMask = new matrix<uint16_t>(xw, zw);

		for (uint16_t i = 0; i < this->m_regionMask->size1(); i++)
			for (uint16_t j = 0; j < this->m_regionMask->size2(); j++)
				(*this->m_regionMask)(i, j) = 0;

		this->m_terrainHeight = y_height;
		this->m_terrainXZGrid = xz_grid;

		this->m_positions = new vector<float>(4 * 3 * 2 * xw * zw);
		this->m_normals = new vector<float>(4 * 3 * 2 * xw * zw);

		this->m_isSigned = true;

		this->m_terrainSet = true;
	}

	return m_status;
}

// ______________________________________________________________________________________________________
// NOISE
GeneratorStatus Terrain::genHeightMap(Terrain::NoiseSettings *m_noiseSettings)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!m_noiseSettings)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_ALREADY_CREATED;
	else
	{
		Terrain::NoiseSettings::NoiseType m_noiseType = m_noiseSettings->getType();

		// Value Noise
		if (m_noiseType == Terrain::NoiseSettings::NoiseType::Value)
		{
			if ((!m_noiseSettings->getOctaves()) || (m_noiseSettings->getLacunarity() <= 0.0f) || (m_noiseSettings->getGain() > 1.0f))
				m_status = GeneratorStatus::GEN_BAD_VALUE;
			else
			{
				ValueNoise2D m_valueNoise(this->m_heightMap->size1(),
										  this->m_heightMap->size2(),
										  m_noiseSettings->getSeed(),
										  this->m_isSigned,
										  m_noiseSettings->getLacunarity(),
										  m_noiseSettings->getGain(),
										  m_noiseSettings->getOctaves(),
										  m_noiseSettings->getInterpolationType());
				m_status = m_valueNoise.makeNoise(this->m_heightMap);
			}
		}
		else
		// Gradient Noise
		if (m_noiseType == Terrain::NoiseSettings::NoiseType::Gradient)
		{
			if (m_noiseSettings->getGradientType() == Terrain::NoiseSettings::GradientNoise::Perlin)
			{
				// TODO
				m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			}
			//else
			//if (m_noiseSettings->getGradientType() == Terrain::NoiseSettings::GradientNoise::Simplex)
			//{
			//	// TODO
			//	m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			//}
			//else
			//if (m_noiseSettings->getGradientType() == Terrain::NoiseSettings::GradientNoise::Wavelet)
			//{
			//	// TODO
			//	m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			//}
			else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
		}
		else
		// Displacement Noise
		if (m_noiseType == Terrain::NoiseSettings::NoiseType::Displacement)
		{
			if ((m_noiseSettings->getHardness() <= 0.0f) || (m_noiseSettings->getHardness() > 1.0f))
				m_status = GeneratorStatus::GEN_BAD_VALUE;
			else
			{
				Terrain::NoiseSettings::DisplacementNoise m_displacementType = m_noiseSettings->getDisplacementType();

				if (m_displacementType == Terrain::NoiseSettings::DisplacementNoise::MidpointDisplacement)
				{
					MidpointDisplacementNoise2D m_midpointDisplacementNoise(this->m_heightMap->size1(),
																			this->m_heightMap->size2(),
																			m_noiseSettings->getSeed(),
																			this->m_isSigned,
																			m_noiseSettings->getHardness());
					m_status = m_midpointDisplacementNoise.makeNoise(this->m_heightMap);
				}
				//else
				//if (m_displacementType == Terrain::NoiseSettings::DisplacementNoise::DiamondSquare)
				//{
				//	// TODO
				//	m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
				//}
				else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			}
		}
		else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;

		if (m_status == GeneratorStatus::GEN_NO_ERROR)
		{
			*this->m_heightMap = *this->m_heightMap * this->m_terrainHeight;
			this->m_noiseSet = true;
		}
	}

	return m_status;
}
// ______________________________________________________________________________________________________

// ______________________________________________________________________________________________________
// EROSION
GeneratorStatus Terrain::erode(Terrain::ErosionSettings *m_erosionSettings)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!m_erosionSettings)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (!this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_NOT_YET_CREATED;
	else
	{
		Terrain::ErosionSettings::ErosionType m_erosionType = m_erosionSettings->getErosionType();

		//if (m_erosionType == Terrain::ErosionSettings::ErosionType::Hidraulic)
		//{
		//	m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
		//}
		//else
		if (m_erosionType == Terrain::ErosionSettings::ErosionType::Thermal)
		{
			if ((!m_erosionSettings->getIterations()) || (m_erosionSettings->getTalus() <= 0.0f))
				m_status = GeneratorStatus::GEN_BAD_VALUE;
			else
			{
				Terrain::ErosionSettings::ThermalErosion m_thermalErosionType = m_erosionSettings->getThermalErosionType();

				if (m_thermalErosionType == Terrain::ErosionSettings::ThermalErosion::Standard)
				{
					m_status = Erosion::getInstance()->thermalErode(this->m_heightMap,
																	m_erosionSettings->getIterations(),
																	m_erosionSettings->getTalus());
				}
				else
				if (m_thermalErosionType == Terrain::ErosionSettings::ThermalErosion::Improved)
				{
					m_status = Erosion::getInstance()->improvedErode(this->m_heightMap,
																	 m_erosionSettings->getIterations(),
																	 m_erosionSettings->getTalus());
				}
				else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			}
		}
		else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
	}

	return m_status;
}
// ______________________________________________________________________________________________________

// ______________________________________________________________________________________________________
// Allocation
std::vector<Terrain::Candidate> Terrain::getCityCandidates(uint16_t seed,
														   GeneratorStatus *status)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;
	
	std::vector<Terrain::Candidate> m_cityCandidates;

	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (!this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_NOT_YET_CREATED;
	else
	if (this->m_citySet)
		m_status = GeneratorStatus::GEN_CITIES_ALREADY_ALLOCATED;
	else
	{
		srand(seed);

		// Divide terrain by 4 and set margin for smoothstep
		// based on the smallest parcel size
		uint16_t m_parcelSize = glm::min((uint16_t)this->m_heightMap->size1() / 2.0f, (uint16_t)this->m_heightMap->size2() / 2.0f),
				 m_outerBorder = (uint16_t)m_parcelSize / 8.0f;

		// For each parcel allocate space where a city may be build
		uint16_t candidates = 0,
				 step = 0;

		while (candidates < 4)
		{
			uint16_t minX = ((candidates % 2) * (0.5f * this->m_heightMap->size1())) + m_outerBorder,
					 maxX = this->m_heightMap->size1() * (0.5f + (0.5f * (candidates % 2))) - m_outerBorder,

					 minZ = ((step % 2) * (0.5f * this->m_heightMap->size2())) + m_outerBorder,
					 maxZ = this->m_heightMap->size2() * (0.5f + (0.5f * (step % 2))) - m_outerBorder;

			glm::vec3 center = glm::vec3((float)(rand() % (int)(maxX - minX)) + minX,
										 0.0f,
										 (float)(rand() % (maxZ - minZ)) + minZ);

			center.y = (*this->m_heightMap)(center.x, center.z);
			float radius = (float)((rand()%(int)((m_parcelSize - (2 * m_outerBorder)) / 2.0f)) + ((m_parcelSize - (2 * m_outerBorder)) / 4.0f));

			// If contained completely within its parcel of the map
			// and its diameter is bigger os equal to 1/4 of the parcel size
			if (((center.x - radius) >= minX) &&
				((center.x + radius) <= maxX) &&
				((center.z - radius) >= minZ) &&
				((center.z + radius) <= maxZ))
			{
					m_cityCandidates.push_back(Candidate(center,
														 radius,
														 radius + m_outerBorder));
				if (candidates % 2)
					step++;
				candidates++;
			}
		}
	}
	status = &m_status;

	return m_cityCandidates;
}

std::map<Terrain::Candidate *, VSResModelLib *> *Terrain::getForest()
{
	return &(this->m_forest);
}

std::vector<Terrain::Candidate> Terrain::getForestCandidates(uint16_t seed,
															 Terrain::NoiseSettings::Density density,
															 GeneratorStatus *status)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;
	
	std::vector<Terrain::Candidate> m_forestCandidates;

	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (!this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_NOT_YET_CREATED;
	else
	if (!this->m_citySet)
		m_status = GeneratorStatus::GEN_CITIES_NOT_YET_ALLOCATED;
	else
	if (!this->m_riverSet)
		m_status = GeneratorStatus::GEN_RIVER_NOT_YET_ALLOCATED;
	else
	if (this->m_forestSet)
		m_status = GeneratorStatus::GEN_FOREST_ALREADY_ALLOCATED;
	else
	{
		srand(seed);

		Terrain::NoiseSettings::Density m_density = (Terrain::NoiseSettings::Density)((rand() % Terrain::NoiseSettings::Density::VERY_HIGH) + 1);
		if (density)
			m_density = density;

		// Calculate amount from density
		uint16_t amount = (uint16_t)((m_density /
									 (float)(Terrain::NoiseSettings::Density::VERY_HIGH - Terrain::NoiseSettings::Density::VERY_LOW)) *
									((this->m_heightMap->size1() + this->m_heightMap->size2())));

		while (amount)
		{
			// Generate random locations and radius
			int32_t i = (uint16_t)rand() % (this->m_heightMap->size1() - 1),
					j = (uint16_t)rand() % (this->m_heightMap->size2() - 1);
			float inner_radius = (uint16_t)(rand() % (int)(M_FOREST_ELEMENT_MAX_RADIUS));

			int32_t minX = (int32_t)(i - (inner_radius + M_FOREST_ELEMENT_OUTER_BORDER)),
					maxX = (int32_t)(i + (inner_radius + M_FOREST_ELEMENT_OUTER_BORDER)),
					minZ = (int32_t)(j - (inner_radius + M_FOREST_ELEMENT_OUTER_BORDER)),
					maxZ = (int32_t)(j + (inner_radius + M_FOREST_ELEMENT_OUTER_BORDER));

			// If within map, and with it's height lower than 3/4 of the total height, add to candidates
			 if ((minX > 50) && (maxX < this->m_heightMap->size1() - 50) &&
                (minZ > 50) && (maxZ < this->m_heightMap->size2() - 50) &&
				((*this->m_heightMap)((uint16_t)i, (uint16_t)j) < (((3 / 4.0f) * this->m_terrainHeight) - (this->m_isSigned?this->m_terrainHeight / 2.0f:0.0f))))
			{
				m_forestCandidates.push_back(Candidate(glm::vec3((uint16_t)i, (*this->m_heightMap)((uint16_t)i, (uint16_t)j), (uint16_t)j),
													   inner_radius,
													   (inner_radius + M_FOREST_ELEMENT_OUTER_BORDER)));
				amount--;
			}
		}
	}
	status = &m_status;

	return m_forestCandidates;
}

GeneratorStatus Terrain::setupCity(std::vector<Terrain::Candidate> m_futureCities)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (!this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_NOT_YET_CREATED;
	else
	if (this->m_citySet)
		m_status = GeneratorStatus::GEN_CITIES_ALREADY_ALLOCATED;
	else
	{
		for (int c = 0; c < m_futureCities.size(); c++)
		{
			uint16_t i_min = m_futureCities.at(c).center.x - m_futureCities.at(c).outerRadius,
					 i_max = m_futureCities.at(c).center.x + m_futureCities.at(c).outerRadius,
					 j_min = m_futureCities.at(c).center.z - m_futureCities.at(c).outerRadius,
					 j_max = m_futureCities.at(c).center.z + m_futureCities.at(c).outerRadius;

			// Generate wheighed mask
			for (uint16_t i = i_min; i <= i_max; i++)
				for (uint16_t j = j_min; j <= j_max; j++)
					wheighedPolarDistanceXY(RegionType::City,
											&i,
											&(*this->m_heightMap)(i, j),
											&j,
											m_futureCities.at(c).center,
											m_futureCities.at(c).innerRadius,
											m_futureCities.at(c).outerRadius);
		}
		this->m_citySet = true;
	}

	return m_status;
}

GeneratorStatus Terrain::setupForest(uint16_t seed,
									 std::vector<Terrain::Candidate> m_futureForestElements)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	srand(seed);

	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (!this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_NOT_YET_CREATED;
	else
	if (!this->m_citySet)
		m_status = GeneratorStatus::GEN_CITIES_NOT_YET_ALLOCATED;
	else
	if (!this->m_riverSet)
		m_status = GeneratorStatus::GEN_RIVER_NOT_YET_ALLOCATED;
	else
	if (this->m_forestSet)
		m_status = GeneratorStatus::GEN_FOREST_ALREADY_ALLOCATED;
	else
	{
		//for (int i = 0; i < M_TREE_AMOUNT; i++)
		//{
		//	this->m_trees.push_back(VSResModelLib());

		//	std::string filename;
		//	filename.append("models/tree");
		//	filename.append(std::to_string(i));
		//	filename.append(".3ds");

		//	if (this->m_trees[this->m_trees.size() - 1].load(filename))
		//	{
		//		this->m_trees[this->m_trees.size() - 1].setMaterialBlockName("Materials");
		//	}
		//}

		this->m_trees.push_back(VSResModelLib());
		if (this->m_trees[this->m_trees.size() - 1].load("models/tree1.3ds"))
		{
			this->m_trees[this->m_trees.size() - 1].setMaterialBlockName("Materials");
		}

		// Because of its totally random nature, it can only be set
		// after River and City spaces have been definitely allocated
		// so that no interference is generated thereon
		for (int c = 0; c < m_futureForestElements.size(); c++)
		{
			uint16_t x = (uint16_t)m_futureForestElements.at(c).center.x,
					 z = (uint16_t)m_futureForestElements.at(c).center.z;
			if (!detectPolarColision(this->m_regionMask,
										&x,
										&z,
										&m_futureForestElements.at(c).outerRadius))
			{
				uint16_t i_min = m_futureForestElements.at(c).center.x - m_futureForestElements.at(c).outerRadius,
						 i_max = m_futureForestElements.at(c).center.x + m_futureForestElements.at(c).outerRadius,
						 j_min = m_futureForestElements.at(c).center.z - m_futureForestElements.at(c).outerRadius,
						 j_max = m_futureForestElements.at(c).center.z + m_futureForestElements.at(c).outerRadius;

				// Generate wheighed mask
				for (uint16_t i = i_min; i <= i_max; i++)
					for (uint16_t j = j_min; j <= j_max; j++)
						wheighedPolarDistanceXY(RegionType::Forest,
												&i,
												&(*this->m_heightMap)(i, j),
												&j,
												m_futureForestElements.at(c).center,
												m_futureForestElements.at(c).innerRadius,
												m_futureForestElements.at(c).outerRadius);
				m_tree_space.push_back(m_futureForestElements.at(c));
				m_forest.insert(std::make_pair(&this->m_tree_space[this->m_tree_space.size() - 1],
//											   &this->m_trees[rand() % (M_TREE_AMOUNT - 1)]));
											   &this->m_trees[0]));
			}
		}
		this->m_forestSet = true;
	}

	return m_status;
}

GeneratorStatus Terrain::setupRiver(Terrain::NoiseSettings *m_noiseSettings)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!m_noiseSettings)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	if (!this->m_noiseSet)
		m_status = GeneratorStatus::GEN_HEIGHTMAP_NOT_YET_CREATED;
	else
	if (this->m_riverSet)
		m_status = GeneratorStatus::GEN_RIVER_ALREADY_ALLOCATED;
	else
	{
		vector<float> m_riverPattern(this->m_heightMap->size2());
		float m_riverThickness = (glm::min((uint16_t)this->m_heightMap->size1() / 2.0, (uint16_t)this->m_heightMap->size2() / 2.0) / 8.0f) * 2.0f;

		GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

		Terrain::NoiseSettings::NoiseType m_noiseType = m_noiseSettings->getType();

		// Value Noise
		if (m_noiseType == Terrain::NoiseSettings::NoiseType::Value)
		{
			if ((!m_noiseSettings->getOctaves()) || (m_noiseSettings->getLacunarity() <= 0.0f) || (m_noiseSettings->getGain() > 1.0f))
				m_status = GeneratorStatus::GEN_BAD_VALUE;
			else
			{
				ValueNoise1D m_valueNoise(m_riverPattern.size(),
										  m_noiseSettings->getSeed(),
										  this->m_isSigned,
										  m_noiseSettings->getLacunarity(),
										  m_noiseSettings->getGain(),
										  m_noiseSettings->getOctaves(),
										  m_noiseSettings->getInterpolationType());
				m_status = m_valueNoise.makeNoise(&m_riverPattern);
			}
		}
		else
		// Gradient Noise
		if (m_noiseType == Terrain::NoiseSettings::NoiseType::Gradient)
		{
			if (m_noiseSettings->getGradientType() == Terrain::NoiseSettings::GradientNoise::Perlin)
			{
				// TODO
				m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			}
			//else
			//if (m_noiseSettings->getGradientType() == Terrain::NoiseSettings::GradientNoise::Simplex)
			//{
			//	// TODO
			//	m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			//}
			//else
			//if (m_noiseSettings->getGradientType() == Terrain::NoiseSettings::GradientNoise::Wavelet)
			//{
			//	// TODO
			//	m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			//}
			else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
		}
		else
		// Displacement Noise
		if (m_noiseType == Terrain::NoiseSettings::NoiseType::Displacement)
		{
			if ((m_noiseSettings->getHardness() <= 0.0f) || (m_noiseSettings->getHardness() > 1.0f))
				m_status = GeneratorStatus::GEN_BAD_VALUE;
			else
			{
				Terrain::NoiseSettings::DisplacementNoise m_displacementType = m_noiseSettings->getDisplacementType();

				if (m_displacementType == Terrain::NoiseSettings::DisplacementNoise::MidpointDisplacement)
				{
					MidpointDisplacementNoise1D m_midpointDisplacementNoise(m_riverPattern.size(),
																			m_noiseSettings->getSeed(),
																			this->m_isSigned,
																			m_noiseSettings->getHardness());
					m_status = m_midpointDisplacementNoise.makeNoise(&m_riverPattern);
				}
				//else
				//if (m_displacementType == Terrain::NoiseSettings::DisplacementNoise::DiamondSquare)
				//{
				//	// TODO
				//	m_status == GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
				//}
				else m_status = GeneratorStatus::GEN_FUNCTION_UNAVAILABLE;
			}
		}

		if (m_status == GeneratorStatus::GEN_NO_ERROR)
		{
			uint16_t min_i = (this->m_heightMap->size1() / 2.0f) - (m_riverThickness / 2.0f),
					 max_i = (this->m_heightMap->size1() / 2.0f) + (m_riverThickness / 2.0f);
			for (uint16_t i = min_i; i <= max_i; i++)
				for (uint16_t j = 0; j < this->m_heightMap->size2(); j++)
					this->wheighedLinearDistanceX(RegionType::River,
													&i,
													&(*this->m_heightMap)(i,j),
													&j,
													this->m_heightMap->size1(),
													m_riverPattern(j),
													(m_riverThickness / 2.0f));
			this->m_riverSet = true;
		}
	}
	return m_status;
}

void Terrain::wheighedPolarDistanceXY(RegionType type, uint16_t *x, float *y, uint16_t *z, glm::vec3 center, float innerRadius, float outerRadius)
{
	float m_distance = sqrt(powf(*x - center.x, 2.0) + powf(*z - center.z, 2.0));

	if (m_distance <= outerRadius)
	{
		if (m_distance > innerRadius)
			*y = Interpolation::getInstance()->smoothStep(center.y, *y, (m_distance - innerRadius)/(outerRadius - innerRadius));
		else *y = center.y;
		(*this->m_regionMask)(*x, *z) = type;
	}
}

void Terrain::wheighedLinearDistanceX(RegionType type, uint16_t *x, float *y, uint16_t *z, uint16_t width, float point, float margin)
{
	if ((((width / 2.0f) + margin) >= *x) && ((width / 2.0f) < *x))
		
	{
		*y = Interpolation::getInstance()->smoothStep((this->m_isSigned?*y - ((((this->m_heightMap->size1() + this->m_heightMap->size2()) / 2.0f)
																			 / (float)this->m_terrainHeight) * (this->m_terrainHeight / 8.0f)):0.0f),
													  *y,
													  ((*x + (point * ((0.5f * margin) / 8.0f))) - (width / 2.0f)) / (float)margin);
		(*this->m_regionMask)(*x,*z) = type;
	}

	if ((((width / 2.0f) - margin) <= *x) && ((width / 2.0f) >= *x))
	{
		*y = Interpolation::getInstance()->smoothStep(*y,
													  (this->m_isSigned?*y - ((((this->m_heightMap->size1() + this->m_heightMap->size2()) / 2.0f)
																			 / (float)this->m_terrainHeight) * (this->m_terrainHeight / 8.0f)):0.0f),
													  ((*x + (point * ((0.5f * margin) / 8.0f)) + margin) - (width / 2.0f)) / (float)margin);
		(*this->m_regionMask)(*x,*z) = type;
	}
}

bool Terrain::detectPolarColision(matrix<uint16_t> *m, uint16_t *x, uint16_t *y, float *radius)
{
	bool result = false;

	int angle = 1;
	do
	{
		if ((*m)((uint16_t)(*x + (*radius * cosf(((float)angle * M_PI) / 180.0f))),
				 (uint16_t)(*y + (*radius * sinf(((float)angle * M_PI) / 180.0f)))))
			result = true;
		angle++;
	}
	while ((angle < 360) && !result);

	return result;
}

GeneratorStatus Terrain::generateTotallyRandom(uint16_t seed,
											   uint16_t widthX,
											   uint16_t widthZ,
											   uint16_t heightY)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	m_status = this->init(seed, true, widthX, widthZ, 1.0f, heightY);

	if (m_status == GeneratorStatus::GEN_NO_ERROR)
	{
		Terrain::NoiseSettings m_terrainNoise(seed, (Terrain::NoiseSettings::PredefinedNoise)
													(rand() % Terrain::NoiseSettings::PredefinedNoise::MIDPOINT_DISPLACEMENT_HIGH_NOISE));
		m_status = this->genHeightMap(&m_terrainNoise);
	}
	if (m_status == GeneratorStatus::GEN_NO_ERROR)
	{
		Terrain::NoiseSettings m_riverNoise(seed, (Terrain::NoiseSettings::PredefinedNoise)
												  (rand() % Terrain::NoiseSettings::PredefinedNoise::MIDPOINT_DISPLACEMENT_HIGH_NOISE));
		m_status = this->setupRiver(&m_riverNoise);
	}
	if (m_status == GeneratorStatus::GEN_NO_ERROR)
	{
		Terrain::ErosionSettings m_erosion((Terrain::ErosionSettings::ThermalErosion)(rand() % Terrain::ErosionSettings::ThermalErosion::Improved),
											(float)(heightY / 2.0f) / (float)((widthX + widthZ) / 2.0f),
											(uint16_t)((100.0f / (float)heightY) * (25 * (uint16_t)(((widthX + widthZ) / 2.0f) / (float)heightY))));
		m_status = this->erode(&m_erosion);
	}

	return m_status;
}

// ______________________________________________________________________________________________________

Terrain::BufferData Terrain::getBuffers(GeneratorStatus *status)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!this->m_terrainSet)
		m_status = GeneratorStatus::GEN_TERRAIN_NOT_YET_ALLOCATED;
	else
	{
		// Generate scaled Vertex object (just to make it easier to compute normals)
		matrix<Vertex> m_mesh(this->m_heightMap->size1(), this->m_heightMap->size2());
		for (uint16_t i = 0; i < this->m_heightMap->size1(); i++)
			for (uint16_t j = 0; j < this->m_heightMap->size2(); j++)
			{
				m_mesh(i, j).position = glm::vec3((float)i * this->m_terrainXZGrid,
												  ((float)(*this->m_heightMap)(i, j)),
												  (float)j * this->m_terrainXZGrid);
				m_mesh(i, j).normal = glm::vec3(0.0f, 0.0f, 0.0f);
			}

		// Compute vertex normals
		for (uint16_t i = 0; i < m_mesh.size1(); i++)
			for (uint16_t j = 0; j < m_mesh.size2(); j++)
			{
				if (((i - 1) > 0) && ((j - 1) > 0))
					m_mesh(i, j).normal += vec3crossProduct(m_mesh(i, j - 1).position - m_mesh(i, j).position,
															m_mesh(i - 1, j).position - m_mesh(i, j).position);
				if (((i - 1) > 0) && ((j + 1) < m_mesh.size2()))
				{
					m_mesh(i, j).normal += vec3crossProduct(m_mesh(i - 1, j).position - m_mesh(i, j).position,
															m_mesh(i - 1, j + 1).position - m_mesh(i, j).position);
					m_mesh(i, j).normal += vec3crossProduct(m_mesh(i - 1, j + 1).position - m_mesh(i, j).position,
															m_mesh(i, j + 1).position - m_mesh(i, j).position);
				}
				if (((i + 1) < m_mesh.size1()) && ((j - 1) > 0))
				{
					m_mesh(i, j).normal += vec3crossProduct(m_mesh(i + 1, j).position - m_mesh(i, j).position,
															m_mesh(i + 1, j - 1).position - m_mesh(i, j).position);
					m_mesh(i, j).normal += vec3crossProduct(m_mesh(i + 1, j - 1).position - m_mesh(i, j).position,
															m_mesh(i, j - 1).position - m_mesh(i, j).position);
				}
				if (((i + 1) < m_mesh.size1()) && ((j + 1) < m_mesh.size2()))
					m_mesh(i, j).normal += vec3crossProduct(m_mesh(i, j + 1).position - m_mesh(i, j).position,
															m_mesh(i + 1, j).position - m_mesh(i, j).position);
			}

		// Normalize vertex normals
		for (uint16_t i = 0; i < m_mesh.size1(); i++)
			for (uint16_t j = 0; j < m_mesh.size2(); j++)
				m_mesh(i, j).normal = vec3normalize(m_mesh(i, j).normal);

		// Fill OpenGL buffers
		uint64_t m_counter_1 = 0,
				 m_counter_2 = 0;

		for (uint16_t i = 0; i < m_mesh.size1() - 1; i++)
			for (uint16_t j = 0; j < m_mesh.size2() - 1; j++)
			{
				// TRIANGLE 1 _______________________________________________________________________
				// Vertex 1
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j).position.x;
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j).position.y;
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j).position.z;
				(*this->m_positions)(m_counter_1++) = (float)1.0f;

				(*this->m_normals)(m_counter_2++) = m_mesh(i, j).normal.x;
				(*this->m_normals)(m_counter_2++) = m_mesh(i, j).normal.y;
				(*this->m_normals)(m_counter_2++) = m_mesh(i, j).normal.z;
				(*this->m_normals)(m_counter_2++) = (float)1.0f;

				// Vertex 2
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j + 1).position.x;
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j + 1).position.y;
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j + 1).position.z;
				(*this->m_positions)(m_counter_1++) = (float)1.0f;

				(*this->m_normals)(m_counter_2++) = m_mesh(i, j + 1).normal.x;
				(*this->m_normals)(m_counter_2++) = m_mesh(i, j + 1).normal.y;
				(*this->m_normals)(m_counter_2++) = m_mesh(i, j + 1).normal.z;
				(*this->m_normals)(m_counter_2++) = (float)1.0f;

				// Vertex 3
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j).position.x;
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j).position.y;
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j).position.z;
				(*this->m_positions)(m_counter_1++) = (float)1.0f;

				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j).normal.x;
				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j).normal.y;
				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j).normal.z;
				(*this->m_normals)(m_counter_2++) = (float)1.0f;
				// __________________________________________________________________________________

				// TRIANGLE 2 _______________________________________________________________________
				// Vertex 1
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j).position.x;
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j).position.y;
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j).position.z;
				(*this->m_positions)(m_counter_1++) = (float)1.0f;

				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j).normal.x;
				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j).normal.y;
				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j).normal.z;
				(*this->m_normals)(m_counter_2++) = (float)1.0f;

				// Vertex 2
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j + 1).position.x;
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j + 1).position.y;
				(*this->m_positions)(m_counter_1++) = m_mesh(i, j + 1).position.z;
				(*this->m_positions)(m_counter_1++) = (float)1.0f;

				(*this->m_normals)(m_counter_2++) = m_mesh(i, j + 1).normal.x;
				(*this->m_normals)(m_counter_2++) = m_mesh(i, j + 1).normal.y;
				(*this->m_normals)(m_counter_2++) = m_mesh(i, j + 1).normal.z;
				(*this->m_normals)(m_counter_2++) = (float)1.0f;

				// Vertex 3
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j + 1).position.x;
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j + 1).position.y;
				(*this->m_positions)(m_counter_1++) = m_mesh(i + 1, j + 1).position.z;
				(*this->m_positions)(m_counter_1++) = (float)1.0f;

				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j + 1).normal.x;
				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j + 1).normal.y;
				(*this->m_normals)(m_counter_2++) = m_mesh(i + 1, j + 1).normal.z;
				(*this->m_normals)(m_counter_2++) = (float)1.0f;
				// __________________________________________________________________________________
			}
	}
	status = &m_status;

	return BufferData(&((*this->m_positions)[0]),
					  this->m_positions->size() / 4.0,
					  &((*this->m_normals)[0]),
					  this->m_normals->size() / 4.0);
}
