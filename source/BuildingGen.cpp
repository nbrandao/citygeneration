# include "BuildingGen.h"

BuildingGen::BuildingGen(StreetGen city){
	// Create Matrix
	boost::numeric::ublas::matrix<int> overlay_tmp(city.radius*2 + 26, city.radius*2 + 26);

	std::vector<StreetGen::ProposedStreet *> streets = city.streets;

	for (unsigned i = 0; i < overlay_tmp.size1(); ++ i){
		for (unsigned j = 0; j < overlay_tmp.size2(); ++ j){
			overlay_tmp(i,j) = 0;
		}
	}

	// For each street paint the matrix
	for(std::vector<StreetGen::ProposedStreet *>::iterator street = streets.begin(); street != streets.end(); ++street) {
		Vertex *org = (*street)->org;
		Vertex *dst = (*street)->dst;

		float main_width = Settings::get().getValue("main_street_width");
		float avenue_width = Settings::get().getValue("avenue_width");

		// For streets with vertical orientation
		if((*street)->direction == StreetGen::FRONT || (*street)->direction == StreetGen::BACK){
			// For streets with vertical orientation the X is always the same
			// Convert the X into a space with center on center_x and center_y
			// and position it on the center of the matrix with (+ city.radius)
			int x = (org->x - city.center_x) + city.radius;
			if(org->z < dst->z){
				// FRONT
				for(int i = org->z; i <= dst->z; i++){
					// Same as for the X
					int z = (i - city.center_z) + city.radius;
					if((*street)->type == StreetGen::MAIN){
						// Paint the street plus its the width
						for(int xi = (x-(main_width/2)); xi <= (x+(main_width/2)); xi++){
							overlay_tmp(xi, z) = 1;
						}
					} else {
						// Paint the street plus its the width
						for(int xi = (x-(avenue_width/2)); xi <= (x+(avenue_width/2)); xi++){
							overlay_tmp(xi, z) = 2;
						}
					}
				}
			} else {
				// BACK
				for(int i = dst->z; i <= org->z; i++){
					// Same as for the X
					int z = (i - city.center_z) + city.radius;
					if((*street)->type == StreetGen::MAIN){
						// Paint the street plus its the width
						for(int xi = (x-(main_width/2)); xi <= (x+(main_width/2)); xi++){
							overlay_tmp(xi, z) = 1;
						}
					} else {
						// Paint the street plus its the width
						for(int xi = (x-(avenue_width/2)); xi <= (x+(avenue_width/2)); xi++){
							overlay_tmp(xi, z) = 2;
						}
					}
				}
			}
		} else {
			// For streets with horizontal orientation

			// For streets with horizontal orientation the Z is always the same
			// Convert the Z into a space with center on center_x and center_z
			// and position it on the center of the matrix with (+ city.radius)
			int z = (org->z - city.center_z) + city.radius;
			if(org->x < dst->x){
				for(int i = org->x; i <= dst->x; i++){
					// Same as for the Z
					int x = (i - city.center_x) + city.radius;
					if((*street)->type == StreetGen::MAIN){
						// Paint the street plus its the width
						for(int zi = (z-(main_width/2)); zi <= (z+(main_width/2)); zi++){
							overlay_tmp(x, zi) = 1;
						}
					} else {
						// Paint the street plus its the width
						for(int zi = (z-(avenue_width/2)); zi <= (z+(avenue_width/2)); zi++){
							overlay_tmp(x, zi) = 2;
						}
					}
				}
			} else {
				for(int i = dst->x; i <= org->x; i++){
					// Same as for the Z
					int x = (i - city.center_x) + city.radius;
					if((*street)->type == StreetGen::MAIN){
						// Paint the street plus its the width
						for(int zi = (z-(main_width/2)); zi <= (z+(main_width/2)); zi++){
							overlay_tmp(x, zi) = 1;
						}
					} else {
						// Paint the street plus its the width
						for(int zi = (z-(avenue_width/2)); zi <= (z+(avenue_width/2)); zi++){
							overlay_tmp(x, zi) = 2;
						}
					}
				}
			}
		}
	}

	overlay = overlay_tmp;

	// for (unsigned i = 0; i < overlay.size1 (); ++ i){
	// 	for (unsigned j = 0; j < overlay.size2 (); ++ j){
	// 		std::cout << overlay (i, j);
	// 	}
	// 	std::cout << "|" << std::endl;
	// }

	generateBuildings(city);
}

void BuildingGen::generateBuildings(StreetGen city){
	int it = 2;
	int y = city.center_y;
	while( it > 0){
		for (unsigned i = 14; i < overlay.size1() - 14; ++ i){
			for (unsigned j = 14; j < overlay.size2() - 14; ++ j){
				int building_width_half;
				int building_length_half;
				if(it == 2){
					building_width_half = CityRandom::get().getRandom(6, 8);
					building_length_half = CityRandom::get().getRandom(6, 8);
				} else {
					building_width_half = 4;
					building_length_half = 4;
				}
				int building_height = CityRandom::get().getRandom(5, 30);
				// Street on the right
				if(overlay(i+building_width_half, j) > 0 && BuildingGen::canBuild(i -building_width_half, j-building_length_half, i+building_width_half, j + building_length_half)){
					Vertex *org = new Vertex((i + building_width_half + city.center_x) - city.radius, y, (j + building_length_half + city.center_z) - city.radius);
					Vertex *dst = new Vertex((i - building_width_half + city.center_x) - city.radius, y, (j - building_length_half + city.center_z) - city.radius);

					Building *b = new Building(org, dst, building_height);
					buildings.push_back(b);
					loadBuilding(b);
					BuildingGen::updateOverlay(i -building_width_half + 1, j-building_length_half + 1, i+building_width_half + 1, j + building_length_half + 1);
				}
				// Street on the left
				if(overlay(i-building_width_half -1, j) > 0 && BuildingGen::canBuild(i -building_width_half, j-building_length_half, i+building_width_half, j + building_length_half)){
					Vertex *org = new Vertex((i + building_width_half + city.center_x) - city.radius, y, (j + building_length_half + city.center_z) - city.radius);
					Vertex *dst = new Vertex((i - building_width_half + city.center_x) - city.radius, y, (j - building_length_half + city.center_z) - city.radius);

					Building *b = new Building(org, dst, building_height);
					buildings.push_back(b);
					loadBuilding(b);
					BuildingGen::updateOverlay(i -building_width_half + 1, j-building_length_half + 1, i+building_width_half + 1, j + building_length_half + 1);
				}
				// Street on up
				if(overlay(i, j+building_length_half) > 0 && BuildingGen::canBuild(i -building_width_half, j-building_length_half, i+building_width_half, j + building_length_half)){
					Vertex *org = new Vertex((i + building_width_half + city.center_x) - city.radius, y, (j + building_length_half + city.center_z) - city.radius);
					Vertex *dst = new Vertex((i - building_width_half + city.center_x) - city.radius, y, (j - building_length_half + city.center_z) - city.radius);

					Building *b = new Building(org, dst, building_height);
					buildings.push_back(b);
					loadBuilding(b);
					BuildingGen::updateOverlay(i -building_width_half + 1, j-building_length_half + 1, i+building_width_half + 1, j + building_length_half + 1);
				}
				// Street on down
				if(overlay(i, j-building_length_half -1) > 0 && BuildingGen::canBuild(i -building_width_half, j-building_length_half, i+building_width_half, j + building_length_half)){
					Vertex *org = new Vertex((i + building_width_half + city.center_x) - city.radius, y, (j + building_length_half + city.center_z) - city.radius);
					Vertex *dst = new Vertex((i - building_width_half + city.center_x) - city.radius, y, (j - building_length_half + city.center_z) - city.radius);

					Building *b = new Building(org, dst, building_height);
					buildings.push_back(b);
					loadBuilding(b);
					BuildingGen::updateOverlay(i -building_width_half + 1, j-building_length_half + 1, i+building_width_half + 1, j + building_length_half + 1);
				}
			}
		}
		it--;
	}
}

bool BuildingGen::canBuild(int org_x, int org_z, int dst_x, int dst_z){
	for (unsigned i = org_x; i < std::min(dst_x, (int) overlay.size1()); ++ i){
		for (unsigned j = org_z; j < std::min(dst_z, (int) overlay.size2()); ++ j){
			if(overlay(i,j) != 0)
				return false;
		}
	}
	return true;
}

void BuildingGen::updateOverlay(int org_x, int org_z, int dst_x, int dst_z){
	for (unsigned i = org_x; i < std::min(dst_x, (int) overlay.size1()); ++ i){
		for (unsigned j = org_z; j < std::min(dst_z, (int) overlay.size2()); ++ j){
			overlay(i,j) = -1;
		}
	}
}

bool BuildingGen::loadBuilding(Building *b){

	VSResBufferLib *building = b->building;

	if(building->loaded()){
		return true;
	}

	Vertex *org_corner = b->org_corner;
	Vertex *dst_corner = b->dst_corner;
	float height = b->height;

	int size = 36;
	int ip = -1;

	float diff_c[4] = {0.0f, 0.9f, 0.9f, 1.0f};

	float *pos = (float *)malloc(sizeof(float)* size * 4);

	// Base Down
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = dst_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	// Base Up
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = dst_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	// Front
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;

	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;

	// Back
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	// Left
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;

	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = dst_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;

	// Right
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;

	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = dst_corner->z; pos[++ip] = 1.0f;
	pos[++ip] = org_corner->x; pos[++ip] = org_corner->y + height; pos[++ip] = org_corner->z; pos[++ip] = 1.0f;

	


	float normal[] = {
		0.0f,-1.0f,0.0f,1.0f,
		0.0f,-1.0f,0.0f,1.0f,
		0.0f,-1.0f,0.0f,1.0f,

		0.0f,-1.0f,0.0f,1.0f,
		0.0f,-1.0f,0.0f,1.0f,
		0.0f,-1.0f,0.0f,1.0f,


		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,

		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,


		0.0f,0.0f,1.0f,1.0f,
		0.0f,0.0f,1.0f,1.0f,
		0.0f,0.0f,1.0f,1.0f,

		0.0f,0.0f,1.0f,1.0f,
		0.0f,0.0f,1.0f,1.0f,
		0.0f,0.0f,1.0f,1.0f,


		0.0f,0.0f,-1.0f,1.0f,
		0.0f,0.0f,-1.0f,1.0f,
		0.0f,0.0f,-1.0f,1.0f,

		0.0f,0.0f,-1.0f,1.0f,
		0.0f,0.0f,-1.0f,1.0f,
		0.0f,0.0f,-1.0f,1.0f,


		-1.0f,1.0f,0.0f,1.0f,
		-1.0f,1.0f,0.0f,1.0f,
		-1.0f,1.0f,0.0f,1.0f,

		-1.0f,1.0f,0.0f,1.0f,
		-1.0f,1.0f,0.0f,1.0f,
		-1.0f,1.0f,0.0f,1.0f,


		1.0f,1.0f,0.0f,1.0f,
		1.0f,1.0f,0.0f,1.0f,
		1.0f,1.0f,0.0f,1.0f,

		1.0f,1.0f,0.0f,1.0f,
		1.0f,1.0f,0.0f,1.0f,
		1.0f,1.0f,0.0f,1.0f,
	};

	bool suc = building->loadBuffers(pos, size, normal, size);
	if(suc){
		building->setColor(VSResourceLib::DIFFUSE, diff_c);
		return true;
	}
	return false;
}