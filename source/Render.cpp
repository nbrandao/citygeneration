#include "Render.h"
#include "Settings.h"

VSResBufferLib street, terreno;
std::vector<StreetGen> citys;
std::vector<BuildingGen> buildings;

int generation_state;
Terrain m_terrain;
VSResBufferLib myModel;

bool first;
std::vector<Terrain::Candidate> m_candidates;
std::vector<VSResModelLib> m_trees;

uint16_t terrainXW = 1000, terrainZW = 1000;
float terrainYW = 500.0f;

// Buffers preloaded before the render cycle
void preloadBuffers(){
	first = true;
	generation_state = TERRAIN;
	std::random_device seed;
	int seed_val = seed();

	// Terrain
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;
	m_status = m_terrain.generateTotallyRandom(seed_val,
		terrainXW,
		terrainZW,
		terrainYW);


	m_candidates = m_terrain.getCityCandidates(seed_val);
	if (m_candidates.size())
		m_terrain.setupCity(m_candidates);

	std::vector<Terrain::Candidate> m_preliminary_forest = m_terrain.getForestCandidates(seed_val);
	if (m_preliminary_forest.size())
		m_terrain.setupForest(seed_val, m_preliminary_forest);

	Terrain::BufferData m_terrainBuffers = m_terrain.getBuffers();
	bool succ = myModel.loadBuffers(m_terrainBuffers.posCoords,
		m_terrainBuffers.posAmount,
		m_terrainBuffers.normCoords,
		m_terrainBuffers.normAmount);


	float diff_c[4] = {0.6f, 0.6f, 0.6f, 1.0f};
	if (succ) {
		printf("%s\n",myModel.getInfo().c_str());

		myModel.setMaterialBlockName("Materials");
		myModel.setColor(VSResourceLib::DIFFUSE, diff_c);
	}
}


// Render function
void render(){
	// loadTerreno(&terreno, float(Settings::get().getValue("terreno_size")));

	// Generate one this step?
	if(next){
		// next = false;

		switch(generation_state){
			case TERRAIN:
			for(std::vector<Terrain::Candidate>::iterator it = m_candidates.begin(); it != m_candidates.end(); ++it){
				citys.push_back(StreetGen((*it).center.x, (*it).center.y+0.5, (*it).center.z, (*it).innerRadius - 20));
			}
				// citys.push_back(StreetGen(0.0, 1, 0.0, 200));
			generation_state = STREETS;
			case STREETS:
			for(std::vector<StreetGen>::iterator city = citys.begin(); city != citys.end(); ++city) {
					// Generate new streets
				(*city).generate();

					// Is streets generation done?
				if((*city).queue.empty()){
					generation_state = BUILDINGS;
				}
			}
			break;
			case BUILDINGS:
				// Generate Matrix from streets
			for(std::vector<StreetGen>::iterator city = citys.begin(); city != citys.end(); ++city) {
					buildings.push_back(BuildingGen((*city)));
			}
			generation_state = DONE;
			break;
			case DONE: break;
		}
	}

	// terreno.renderBuffers();

	myModel.renderBuffers();
	renderTrees();
	renderStreets();
	renderBuildings();
}

void renderTrees()
{
	std::map<Terrain::Candidate *, VSResModelLib *> *m_forest = m_terrain.getForest();
	for (std::map<Terrain::Candidate *, VSResModelLib *>::iterator forest_it = m_forest->begin(); forest_it != m_forest->end(); forest_it++)
	{
		vsml->pushMatrix(VSMathLib::MODEL);
		vsml->translate(forest_it->first->center.x, forest_it->first->center.y + 0.5f, forest_it->first->center.z);
		vsml->rotate(-90.0f, 1.0f, 0.0f, 1.0f);
		forest_it->second->render();
		vsml->popMatrix(VSMathLib::MODEL);
	}
}

void renderStreets(){
	for(std::vector<StreetGen>::iterator city = citys.begin(); city != citys.end(); ++city) {
		std::vector<StreetGen::ProposedStreet *> streets = city->streets;
		for(std::vector<StreetGen::ProposedStreet *>::iterator street = streets.begin(); street != streets.end(); ++street) {
			bool suc = city->loadStreet(*street);
			if(suc){
				// vsml->pushMatrix(VSMathLib::MODEL);
				(*street)->street->renderBuffers();
				// vsml->pushMatrix(VSMathLib::MODEL);
			}
		}
	}
}

void renderBuildings(){
	for(std::vector<BuildingGen>::iterator building = buildings.begin(); building != buildings.end(); ++building) {
		std::vector<BuildingGen::Building *> bs = building->buildings ;

		for(std::vector<BuildingGen::Building *>::iterator b = bs.begin(); b != bs.end(); ++b) {
			bool suc = building->loadBuilding(*b);
			if(suc){
				// vsml->pushMatrix(VSMathLib::MODEL);
				(*b)->building->renderBuffers();
				// vsml->pushMatrix(VSMathLib::MODEL);
			}
		}
	}
}
