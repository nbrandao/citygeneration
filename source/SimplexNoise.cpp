#include "SimplexNoise.h"

// 1D _____
SimplexNoise1D::SimplexNoise1D(uint16_t w,
							   uint16_t seed,
							   bool isSigned,
							   float persistence,
							   uint16_t octaves,
							   Interpolation::InterpolationType iType) : WhiteNoise1D(w, seed, isSigned)
{
	this->m_persistence = persistence;
	this->m_octaves = octaves;
	this->m_iType = iType;
}

SimplexNoise1D::~SimplexNoise1D(void) {}

// MODIFY TO SIMPLEXNOISE
float SimplexNoise1D::makeNoise(vector<float> *heightMap)
{
	float m_max = FLT_MIN;

	for (uint16_t i = 0; i < heightMap->size(); i++)
	{
		float m_total = 0.0f,
				m_frequency = 1.0f/(float)heightMap->size(),
				m_amplitude = this->m_persistence;
		for (uint16_t o = 0; o < this->m_octaves; o++)
		{
			m_total += this->evaluate(m_iType, i * m_frequency) * m_amplitude;
			m_frequency *= 2;
			m_amplitude *= this->m_persistence;
		}
		(*heightMap)(i) = m_total;

		if (m_total > m_max)
			m_max = m_total;
	}

	return m_max;
}

// 2D _____
SimplexNoise2D::SimplexNoise2D(uint16_t w,
							   uint16_t h,
							   uint16_t seed,
							   bool isSigned,
							   float persistence,
							   uint16_t octaves,
							   Interpolation::InterpolationType iType) : WhiteNoise2D(w, h, seed, isSigned)
{
	this->m_persistence = persistence;
	this->m_octaves = octaves;
	this->m_iType = iType;
}

SimplexNoise2D::~SimplexNoise2D(void) {}

// MODIFY TO SIMPLEXNOISE
float SimplexNoise2D::makeNoise(matrix<float> *heightMap)
{
	float m_max = FLT_MIN;

	for (uint16_t i = 0; i < heightMap->size1(); i++)
		for (uint16_t j = 0; j < heightMap->size2(); j++)
		{
			float m_total = 0.0f,
				  m_frequency = 1.0f/(float)heightMap->size1(),
				  m_amplitude = this->m_persistence;
			for (uint16_t o = 0; o < this->m_octaves; o++)
			{
				m_total += this->evaluate(m_iType, i * m_frequency, j * m_frequency) * m_amplitude;
				m_frequency *= 2;
				m_amplitude *= this->m_persistence;
			}
			(*heightMap)(i, j) = m_total;

			if (m_total > m_max)
				m_max = m_total;
		}

		return m_max;
}