#include "Interpolation.h"

Interpolation *Interpolation::getInstance()
{
	if (!instance)
		instance = new Interpolation();
	return instance;
}
Interpolation *Interpolation::instance = 0;
Interpolation::Interpolation(void) {}
Interpolation::~Interpolation(void) {}

float Interpolation::linearInterpolate(float a, float b, float t)
{
	return (a * (1 - t) + b * t);
}

float Interpolation::cosineInterpolate(float a, float b, float t)
{
	return this->linearInterpolate(a, b, (1 - cosf(t * M_PI)) / 2.0f);
}

float Interpolation::cubicInterpolate(float a, float b, float c, float d, float t)
{
    /*
    # Catmull-Rom doesn't work properly
    p0 = -0.5*a+1.5*b-1.5*c+0.5*d
    p1 = a-(5/2.0)*b+2*c-0.5*d
    p2 = -0.5*a+0.5*b
    p3 = b
    */
    
    float p0 = d - c - a + b;
    float p1 = a - b - p0;
    float p2 = c - a;
    float p3 = b;

	return (p0 * powf(t, 3.0)) + (p1 * powf(t, 2.0)) + (p2 * t) + p3;
}

float Interpolation::smoothStep(float a, float b, float t)
{
	return this->linearInterpolate(a, b, 3 * powf(t, 2.0) - 2 * powf(t,3.0));
}