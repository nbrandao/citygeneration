#include "CityRandom.h"

std::mt19937 CityRandom::Generator;

CityRandom::CityRandom(){
	std::random_device seed;
	Generator.seed(seed());
}

int CityRandom::getRandom(int min, int max){
	return std::uniform_int_distribution<int>(min, max)(Generator);
}

float CityRandom::getRandomFloat(float min, float max){
	return std::uniform_real_distribution<float>(min, max)(Generator);
}