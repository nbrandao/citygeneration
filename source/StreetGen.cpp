#include "StreetGen.h"


bool StreetGen::StreetGenComparison::operator()( const StreetGen::ProposedStreet* s1, const StreetGen::ProposedStreet* s2) const {
	return s1->time > s2->time;
}

StreetGen::StreetGen(){
	// Initialize queue with starting street
	queue.push(new ProposedStreet(0, new Vertex(0.0f,center_y,0.0f), new Vertex(0.0f,center_y,-10.0f), FRONT, FRONT, MAIN, 0));
	queue.push(new ProposedStreet(0, new Vertex(0.0f,center_y,0.0f), new Vertex(0.0f,center_y,+10.0f), BACK, BACK, MAIN, 0));
	queue.push(new ProposedStreet(0, new Vertex(0.0f,center_y,0.0f), new Vertex(-10.0f,center_y,0.0f), LEFT, LEFT, MAIN, 0));
	queue.push(new ProposedStreet(0, new Vertex(0.0f,center_y,0.0f), new Vertex(10.0f,center_y,0.0f), RIGHT, RIGHT,MAIN, 0));

	genCounter = 0;
}

StreetGen::StreetGen(float x, float y, float z, float init_radius){
	center_x = x;
	center_y = y;
	center_z = z;
	radius = init_radius;

	// Initialize queue with starting street
	queue.push(new ProposedStreet(0, new Vertex(x,center_y,z), new Vertex(x,center_y,-10.0f+z), FRONT, FRONT, MAIN, 0));
	queue.push(new ProposedStreet(0, new Vertex(x,center_y,z), new Vertex(x,center_y,+10.0f+z), BACK, BACK, MAIN, 0));
	queue.push(new ProposedStreet(0, new Vertex(x,center_y,z), new Vertex(-10.0f+x,center_y,z), LEFT, LEFT, MAIN, 0));
	queue.push(new ProposedStreet(0, new Vertex(x,center_y,z), new Vertex(10.0f+x,center_y,z), RIGHT, RIGHT,MAIN, 0));

	genCounter = 0;
}

void StreetGen::generate(){
	while(!queue.empty()){
		if(genCounter == 1)
			break;
		// Retrive next elem
		ProposedStreet *ps = queue.top();
		queue.pop();
		bool accepted = localConstraints(ps);
		if(accepted){
			streets.push_back(ps);
			if(!ps->intersect){
				std::vector<ProposedStreet *> new_streets = globalGoals(*ps);
				for(std::vector<ProposedStreet *>::iterator street = new_streets.begin(); street != new_streets.end(); ++street) {
					queue.push(*street);
				}
			}
		}
		genCounter++;
	}
	genCounter = 0;
}

bool StreetGen::localConstraints(ProposedStreet *ps){
	// If outside map generate no more
	if(abs(center_x - ps->dst->x) > radius || abs(center_z - ps->dst->z) > radius){
		return false;
	}

	if(ps->type == StreetGen::AVENUE){
		for(std::vector<StreetGen::ProposedStreet *>::iterator street = streets.begin(); street != streets.end(); ++street) {
			if(StreetGen::parallel(*ps, *(*street))){
				return false;
			}
		}
	}
	return true;
}

std::vector<StreetGen::ProposedStreet *> StreetGen::globalGoals(ProposedStreet ps){
	std::vector<StreetGen::ProposedStreet *> new_streets = std::vector<StreetGen::ProposedStreet *>();

	// If last was a Main Street
	if(ps.type == StreetGen::MAIN){
		// Generate Main Street
		ProposedStreet *to = generateMainStreet(ps);
		new_streets.push_back(to);

		// Generate Avenue from Main Street
		ProposedStreet *a = generateAvenueFromMain(*to);
		if(a != nullptr)
			new_streets.push_back(a);
	} else if(ps.type == StreetGen::AVENUE && ps.total_length <= 20 && !ps.intersect){
		ProposedStreet *a = generateAvenue(ps);
		if(a != nullptr)
			new_streets.push_back(a);
	}

	return new_streets;
}

StreetGen::ProposedStreet* StreetGen::generateMainStreet(StreetGen::ProposedStreet from){
	StreetGen::ProposedStreet *to;

	// Generate direction
	int direction = newDirection(invertDirection(from.direction), invertDirection(from.orientation));

	// Generate street length
	float length = (float) CityRandom::get().getRandom(Settings::get().getValue("main_street_length_min"), Settings::get().getValue("main_street_length_max"));
	switch (direction)
	{
		case StreetGen::FRONT:
		to = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x,from.dst->y,from.dst->z-length), StreetGen::FRONT, from.orientation, StreetGen::MAIN, 0);
		break;
		case StreetGen::LEFT:
		to = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x-length,from.dst->y,from.dst->z), StreetGen::LEFT, from.orientation, StreetGen::MAIN, 0);
		break;
		case StreetGen::RIGHT:
		to = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x + length,from.dst->y,from.dst->z), StreetGen::RIGHT, from.orientation, StreetGen::MAIN, 0);
		break;
		case StreetGen::BACK:
		to = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x,from.dst->y,from.dst->z+length), StreetGen::BACK, from.orientation, StreetGen::MAIN, 0);
		break;
		default:
		to = nullptr;
		break;
	}

	return to;
}

StreetGen::ProposedStreet* StreetGen::generateAvenueFromMain(ProposedStreet main){
	StreetGen::ProposedStreet* avenue = nullptr;

	// Generate length
	float length = (float) CityRandom::get().getRandom(Settings::get().getValue("avenue_length_min"), Settings::get().getValue("avenue_length_max"));
	float main_width = Settings::get().getValue("main_street_width");
	float pos = -1;
	int direction = 0;
	switch (main.direction) {
		case StreetGen::FRONT:
			// Get direction must be different that the current and invert of it
		direction = newDirection(main.direction, invertDirection(main.direction));

			// Get position along the main street
		pos = CityRandom::get().getRandomFloat(main.org->z - main_width, main.dst->z + main_width);

			// LEFT or RIGHT
		if(direction == StreetGen::LEFT)
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(main.org->x, main.org->y+0.2, pos), new Vertex(main.org->x - length, main.org->y+0.2, pos), direction, direction, StreetGen::AVENUE, 1);
		else
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(main.org->x, main.org->y+0.2, pos), new Vertex(main.org->x + length, main.org->y+0.2, pos), direction, direction, StreetGen::AVENUE, 1);
		break;
		case StreetGen::LEFT:
			// Get direction must be different that the current and invert of it
		direction = newDirection(main.direction, invertDirection(main.direction));

			// Get position along the main street
		pos = CityRandom::get().getRandomFloat(main.org->x - main_width, main.dst->x + main_width);

			// FRONT or BACK
		if(direction == StreetGen::FRONT)
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(pos, main.org->y+0.2, main.org->z), new Vertex(pos, main.org->y+0.2, main.org->z - length), direction, direction, StreetGen::AVENUE, 1);
		else
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(pos, main.org->y+0.2, main.org->z), new Vertex(pos, main.org->y+0.2, main.org->z + length), direction, direction, StreetGen::AVENUE, 1);
		break;
		case StreetGen::RIGHT:
			// Get direction must be different that the current and invert of it
		direction = newDirection(main.direction, invertDirection(main.direction));

			// Get position along the main street
		pos = CityRandom::get().getRandomFloat(main.org->x + main_width, main.dst->x - main_width);

			// FRONT or BACK
		if(direction == StreetGen::FRONT)
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(pos, main.org->y+0.2, main.org->z), new Vertex(pos, main.org->y+0.2, main.org->z - length), direction, direction, StreetGen::AVENUE, 1);
		else
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(pos, main.org->y+0.2, main.org->z), new Vertex(pos, main.org->y+0.2, main.org->z + length), direction, direction, StreetGen::AVENUE, 1);
		break;
		break;
		case StreetGen::BACK:
			// Get direction must be different that the current and invert of it
		direction = newDirection(main.direction, invertDirection(main.direction));

			// Get position along the main street
		pos = CityRandom::get().getRandomFloat(main.org->z + main_width, main.dst->z - main_width);

			// LEFT or RIGHT
		if(direction == StreetGen::LEFT)
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(main.org->x, main.org->y+0.2, pos), new Vertex(main.org->x - length, main.org->y+0.2, pos), direction, direction, StreetGen::AVENUE, 1);
		else
			avenue = new StreetGen::ProposedStreet(main.time + 1, new Vertex(main.org->x, main.org->y+0.2, pos), new Vertex(main.org->x + length, main.org->y+0.2, pos), direction, direction, StreetGen::AVENUE, 1);
		break;
		break;
	}

	return avenue;
}

StreetGen::ProposedStreet* StreetGen::generateAvenue(ProposedStreet from){
	StreetGen::ProposedStreet* avenue = nullptr;

	// Generate length
	float length = (float) CityRandom::get().getRandom(Settings::get().getValue("avenue_length_min"), Settings::get().getValue("avenue_length_max"));

	// Generate direction
	int direction = newDirection(invertDirection(from.direction), invertDirection(from.orientation));

	switch (direction)
	{
		case StreetGen::FRONT:
		avenue = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x,from.dst->y,from.dst->z-length), StreetGen::FRONT, from.orientation, StreetGen::AVENUE, from.total_length + 1);
		break;
		case StreetGen::LEFT:
		avenue = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x-length,from.dst->y,from.dst->z), StreetGen::LEFT, from.orientation, StreetGen::AVENUE, from.total_length + 1);
		break;
		case StreetGen::RIGHT:
		avenue = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x + length,from.dst->y,from.dst->z), StreetGen::RIGHT, from.orientation, StreetGen::AVENUE, from.total_length + 1);
		break;
		case StreetGen::BACK:
		avenue = new StreetGen::ProposedStreet(from.time + 1, from.dst, new Vertex(from.dst->x,from.dst->y,from.dst->z+length), StreetGen::BACK, from.orientation, StreetGen::AVENUE, from.total_length + 1);
		break;
	}

	return avenue;
}

int StreetGen::newDirection(int direction1, int direction2){
	// Generate direction
	int direction = CityRandom::get().getRandom(0, 3);

	// Verify if direction is valid
	while (direction == direction1 || direction == direction2)
		direction = CityRandom::get().getRandom(0, 3);

	return direction;
}

int StreetGen::invertDirection(int direction){
	switch (direction)
	{
		case StreetGen::FRONT:
		return StreetGen::BACK;
		case StreetGen::LEFT:
		return StreetGen::RIGHT;
		case StreetGen::RIGHT:
		return StreetGen::LEFT;
		case StreetGen::BACK:
		return StreetGen::FRONT;
	}

	return -1;
}

bool StreetGen::intercept(Vertex P, Vertex Q, Vertex E, Vertex F){
	float A1 = Q.z - P.z;
	float B1 = P.x - Q.x;
	float C1 = A1 * P.x + B1 * P.z;

	float A2 = F.z - E.z;
	float B2 = E.x - F.x;
	float C2 = A2 * E.x + B2 * E.z;

	double det = A1*B2 - A2*B1;

	// Not Parallel
	if(det != 0){
		double x = (B2*C1 - B1*C2)/det;
		double y = (A1*C2 - A2*C1)/det;

        // Exists in PQ
		if(std::min(P.x,Q.x) <= x && x <= std::max(P.x,Q.x) && std::min(P.z,Q.z) <= y && y <= std::max(P.z,Q.z)){
        	// 	Exists in EF
			if(std::min(E.x,F.x) <= x && x <= std::max(E.x,F.x) && std::min(E.z,F.z) <= y && y <= std::max(E.z,F.z)){
				return true;
			}
		}
	}

	return false;
}

bool StreetGen::parallel(ProposedStreet street, ProposedStreet matched){
	Vertex P = *street.org;
	Vertex Q = *street.dst;
	Vertex E = *matched.org;
	Vertex F = *matched.dst;
	// Parallel
	if(isVertical(street.direction) == isVertical(matched.direction) ){
		if( street.direction == StreetGen::LEFT || street.direction == StreetGen::RIGHT){
			if(std::min(P.x, Q.x) > std::min(E.x, F.x) && std::min(P.x, Q.x) < std::max(E.x, F.x) && fabs(P.z - E.z) < Settings::get().getValue("minimum_parallel_size"))
				return true;
			else if(std::max(P.x, Q.x) > std::min(E.x, F.x) && std::max(P.x, Q.x) < std::max(E.x, F.x) && fabs(P.z - E.z) < Settings::get().getValue("minimum_parallel_size"))
				return true;
		} else {
			if(std::min(P.z, Q.z) > std::min(E.z, F.z) && std::min(P.z, Q.z) < std::max(E.z, F.z) && fabs(P.x - E.x) < Settings::get().getValue("minimum_parallel_size"))
				return true;
			else if(std::max(P.z, Q.z) > std::min(E.z, F.z) && std::max(P.z, Q.z) < std::max(E.z, F.z) && fabs(P.x - E.x) < Settings::get().getValue("minimum_parallel_size"))
				return true;
		}
	}
	return false;
}

bool StreetGen::isVertical(int direction){
	if(direction == FRONT || direction == BACK){
		return false;
	}

	return true;
}

bool StreetGen::loadStreet(ProposedStreet *ps){

	VSResBufferLib *street = ps->street;

	if(street->loaded()){
		return true;
	}

	int size = 6;
	int ip = -1;

	float width;
	if(ps->type == StreetGen::MAIN)
		width = Settings::get().getValue("main_street_width");
	else 
		width = Settings::get().getValue("avenue_width");

	width = width/2;
	Vertex org = *(ps->org);
	Vertex dst = *(ps->dst);

	float diff_c[4];

	float *pos = (float *)malloc(sizeof(float)*6*4);

	if(ps->type == StreetGen::MAIN){
		diff_c[0] = 0.9f; diff_c[1] = 0.0f; diff_c[2] = 0.0f; diff_c[3] = 1.0f;
	} else {
		diff_c[0] = 0.0f; diff_c[1] = 0.9f; diff_c[2] = 0.0f; diff_c[3] = 1.0f;
	}

	if (ps->direction == FRONT){
		pos[++ip] = org.x-width; pos[++ip] = org.y; pos[++ip] = org.z; pos[++ip] = 1.0f;
		pos[++ip] = org.x+width; pos[++ip] = org.y; pos[++ip] = org.z; pos[++ip] = 1.0f;
		pos[++ip] = dst.x-width; pos[++ip] = dst.y; pos[++ip] = dst.z; pos[++ip] = 1.0f;

		pos[++ip] = dst.x-width; pos[++ip] = dst.y; pos[++ip] = dst.z; pos[++ip] = 1.0f;
		pos[++ip] = org.x+width; pos[++ip] = org.y; pos[++ip] = org.z; pos[++ip] = 1.0f;
		pos[++ip] = dst.x+width; pos[++ip] = dst.y; pos[++ip] = dst.z; pos[++ip] = 1.0f;

	} else if(ps->direction == RIGHT){
		pos[++ip] = org.x; pos[++ip] = org.y; pos[++ip] = org.z-width; pos[++ip] = 1.0f;
		pos[++ip] = org.x; pos[++ip] = org.y; pos[++ip] = org.z+width; pos[++ip] = 1.0f;
		pos[++ip] = dst.x; pos[++ip] = dst.y; pos[++ip] = dst.z-width; pos[++ip] = 1.0f;

		pos[++ip] = dst.x; pos[++ip] = dst.y; pos[++ip] = dst.z-width; pos[++ip] = 1.0f;
		pos[++ip] = org.x; pos[++ip] = org.y; pos[++ip] = org.z+width; pos[++ip] = 1.0f;
		pos[++ip] = dst.x; pos[++ip] = dst.y; pos[++ip] = dst.z+width; pos[++ip] = 1.0f;
	} else if(ps->direction == BACK){
		pos[++ip] = org.x-width; pos[++ip] = org.y; pos[++ip] = org.z; pos[++ip] = 1.0f;
		pos[++ip] = dst.x-width; pos[++ip] = dst.y; pos[++ip] = dst.z; pos[++ip] = 1.0f;
		pos[++ip] = org.x+width; pos[++ip] = org.y; pos[++ip] = org.z; pos[++ip] = 1.0f;

		pos[++ip] = dst.x-width; pos[++ip] = dst.y; pos[++ip] = dst.z; pos[++ip] = 1.0f;
		pos[++ip] = dst.x+width; pos[++ip] = dst.y; pos[++ip] = dst.z; pos[++ip] = 1.0f;
		pos[++ip] = org.x+width; pos[++ip] = org.y; pos[++ip] = org.z; pos[++ip] = 1.0f;

	} else {
		pos[++ip] = org.x; pos[++ip] = org.y; pos[++ip] = org.z-width; pos[++ip] = 1.0f;
		pos[++ip] = dst.x; pos[++ip] = dst.y; pos[++ip] = dst.z-width; pos[++ip] = 1.0f;
		pos[++ip] = org.x; pos[++ip] = org.y; pos[++ip] = org.z+width; pos[++ip] = 1.0f;

		pos[++ip] = dst.x; pos[++ip] = dst.y; pos[++ip] = dst.z-width; pos[++ip] = 1.0f;
		pos[++ip] = dst.x; pos[++ip] = dst.y; pos[++ip] = dst.z+width; pos[++ip] = 1.0f;
		pos[++ip] = org.x; pos[++ip] = org.y; pos[++ip] = org.z+width; pos[++ip] = 1.0f;

	}


	float normal[] = {
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f
	};

	bool suc = street->loadBuffers(pos, size, normal, size);
	if(suc){
		street->setColor(VSResourceLib::DIFFUSE, diff_c);
		return true;
	}
	return false;
}


