#include "PerlinNoise.h"

uint16_t randomizer(uint16_t i)
{
	return rand() % i;
}

// 1D _____
PerlinNoise1D::PerlinNoise1D(uint16_t width,
							 uint16_t seed,
							 bool isSigned,
							 float lacunarity,
							 float gain,
							 uint16_t octaves,
							 Interpolation::InterpolationType iType)
{
	srand(seed);

	// Fill and randomize the permutations vector
	this->m_permutations = new vector<uint16_t>(M_RANDOM_SIZE);
	for (int i = 0; i < this->m_permutations->size(); i++)
		(*this->m_permutations)(i) = i;
	std::random_shuffle(this->m_permutations->begin(), this->m_permutations->end(), randomizer);

	// Fill the gradient vector
	this->m_gradients = new vector<glm::vec2>((uint16_t)(360 / (float)M_GRADIENT_ANGLE));
	for (int i = 0; i < this->m_gradients->size(); i++)
		(*this->m_gradients)(i) = glm::vec2(cosf((i * (360 / (float)M_GRADIENT_ANGLE)) * (180 / (float) M_PI)),
											sinf((i * (360 / (float)M_GRADIENT_ANGLE)) * (180 / (float) M_PI)));

	uint16_t m_iterations = -1,
			 m_power = 0;

	do m_iterations += 1;
	while ((m_power = (uint16_t)pow(2, m_iterations)) < width);

	this->m_heightMap = new vector<float>(m_power, m_power);
	for (uint16_t c = 0; c < this->m_heightMap->size(); c++)
		(*this->m_heightMap)(c) = 0.0f;

	this->m_isSigned = isSigned;
	this->m_maxVal = FLT_MIN;
	this->m_minVal = FLT_MAX;

	for (uint16_t i = 1; i < this->m_heightMap->size() - 1; i++)
	{
		float m_total = 0.0f,
				m_frequency = 1.0f / (float)(this->m_heightMap->size()),
				m_amplitude = gain;

		for (int o = 0; o < octaves; o++)
		{
			m_total += this->evaluate(iType, i * m_frequency) * m_amplitude;
			m_frequency *= lacunarity;
			m_amplitude *= gain;
		}
		(*this->m_heightMap)(i) = m_total;

		this->m_maxVal = glm::max((*this->m_heightMap)(i), this->m_maxVal);
		this->m_minVal = glm::min((*this->m_heightMap)(i), this->m_minVal);
	}
}

PerlinNoise1D::~PerlinNoise1D(void)
{
	delete this->m_permutations;
}

glm::vec2 PerlinNoise1D::gradientX(uint16_t x)
{
	return (*this->m_gradients)((*this->m_permutations)(x % (this->m_permutations->size() - 1)) % (this->m_gradients->size() - 1));
}

float PerlinNoise1D::evaluate(Interpolation::InterpolationType iType, float x)
{
	float result = 0.0f;

	int x_int = (int)x;

	glm::vec2 grad0 = this->gradientX(x_int),
			  grad1 = this->gradientX(x_int + 1);

	if (iType == Interpolation::Linear)
	{
		float v1 = (grad0.y / (float)grad0.x) * ((x - (float)x_int) - 0.0f),		// (0)
			  v2 = (grad1.y / (float)grad1.x) * ((x - (float)x_int) - 1.0f);		// (1)

		result = Interpolation::getInstance()->linearInterpolate(v1, v2, x - (float)x_int);		// Interpolate values @ X axis
	}

	if (iType == Interpolation::Cosine)
	{
		float v1 = (grad0.y / (float)grad0.x) * ((x - (float)x_int) - 0.0f),		// (0)
			  v2 = (grad1.y / (float)grad1.x) * ((x - (float)x_int) - 1.0f);		// (1)

		result = Interpolation::getInstance()->cosineInterpolate(v1, v2, x - (float)x_int);		// Interpolate values @ X axis
	}

	if (iType == Interpolation::Cubic)
	{
		float v1 = (grad0.y / (float)grad0.x) * ((x - (float)x_int) - 1.0f),		// (-1)
			  v2 = (grad0.y / (float)grad0.x) * ((x - (float)x_int) - 0.0f),		// (0)
			  v3 = (grad1.y / (float)grad1.x) * ((x - (float)x_int) - 1.0f),		// (1)
			  v4 = (grad1.y / (float)grad1.x) * ((x - (float)x_int) - 0.0f);		// (2)

		result = Interpolation::getInstance()->cubicInterpolate(v1, v2, v3, v4, x - (float)x_int); 		// Interpolate values @ X axis
	}

	if (iType == Interpolation::SmoothStep)
	{
		float v1 = (grad0.y / (float)grad0.x) * ((x - (float)x_int) - 0.0f),		// (0)
			  v2 = (grad1.y / (float)grad1.x) * ((x - (float)x_int) - 1.0f);		// (1)

		result = Interpolation::getInstance()->smoothStep(v1, v2, x - (float)x_int);		// Interpolate values @ X axis
	}

	return result;
}

GeneratorStatus PerlinNoise1D::makeNoise(vector<float> *heightMap)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if (this->m_heightMap->size() < heightMap->size())
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	{
		uint16_t start_i = (uint16_t)((this->m_heightMap->size() - heightMap->size()) / 2.0f);

		for (uint16_t i = 0; i < heightMap->size(); i++)
			(*heightMap)(i) = (((*this->m_heightMap)(i + start_i) - this->m_minVal) / (float)(this->m_maxVal - this->m_minVal)) - (this->m_isSigned?0.5f:0.0f);
	}

	return m_status;
}


// 2D _____
PerlinNoise2D::PerlinNoise2D(uint16_t width,
							 uint16_t height,
							 uint16_t seed,
							 bool isSigned,
							 float lacunarity,
							 float gain,
							 uint16_t octaves,
							 Interpolation::InterpolationType iType)
{
	srand(seed);

	// Fill and randomize the permutations vector
	this->m_permutations = new vector<uint16_t>(M_RANDOM_SIZE);
	for (int i = 0; i < this->m_permutations->size(); i++)
		(*this->m_permutations)(i) = i;
	std::random_shuffle(this->m_permutations->begin(), this->m_permutations->end(), randomizer);

	// Fill the gradient vector
	this->m_gradients = new vector<glm::vec2>((uint16_t)(360 / (float)M_GRADIENT_ANGLE));
	for (int i = 0; i < this->m_gradients->size(); i++)
		(*this->m_gradients)(i) = glm::vec2(cosf((i * (360 / (float)M_GRADIENT_ANGLE)) * (180 / (float) M_PI)),
											sinf((i * (360 / (float)M_GRADIENT_ANGLE)) * (180 / (float) M_PI)));

	uint16_t m_iterations = -1,
			 m_power = 0,
			 m_minSize = glm::max(width, height);

	do m_iterations += 1;
	while ((m_power = (uint16_t)pow(2, m_iterations)) < m_minSize);

	this->m_heightMap = new matrix<float>(m_power, m_power);
	for (uint16_t c = 0; c < this->m_heightMap->size1(); c++)
		(*this->m_heightMap)(c, c) = 0.0f;

	this->m_isSigned = isSigned;
	this->m_maxVal = FLT_MIN;
	this->m_minVal = FLT_MAX;

	for (uint16_t i = 1; i < this->m_heightMap->size1() - 1; i++)
		for (uint16_t j = 1; j < this->m_heightMap->size2() - 1; j++)
		{
			float m_total = 0.0f,
				  m_frequency = 1.0f / (float)((this->m_heightMap->size1() + this->m_heightMap->size2()) / 2.0f),
				  m_amplitude = gain;

			for (int o = 0; o < octaves; o++)
			{
				m_total += this->evaluate(iType, i * m_frequency, j * m_frequency) * m_amplitude;
				m_frequency *= lacunarity;
				m_amplitude *= gain;
			}
			(*this->m_heightMap)(i, j) = m_total;

			this->m_maxVal = glm::max((*this->m_heightMap)(i, j), this->m_maxVal);
			this->m_minVal = glm::min((*this->m_heightMap)(i, j), this->m_minVal);
		}
}

PerlinNoise2D::~PerlinNoise2D(void)
{
	delete this->m_permutations;
}

glm::vec2 PerlinNoise2D::gradientXY(uint16_t x, uint16_t y)
{
	return (*this->m_gradients)((*this->m_permutations)((x + (*this->m_permutations)(y %
														 (this->m_permutations->size() - 1)))
														 % (this->m_permutations->size() - 1))
														 % (this->m_gradients->size() - 1));
}

float PerlinNoise2D::evaluate(Interpolation::InterpolationType iType, float x, float y)
{
	float result = 0.0f;

	int x_int = (int)x,
		y_int = (int)y;

	glm::vec2 grad00 = this->gradientXY(x_int, y_int),
			  grad10 = this->gradientXY(x_int + 1, y_int),
			  grad01 = this->gradientXY(x_int, y_int + 1),
			  grad11 = this->gradientXY(x_int + 1, y_int + 1);

	if (iType == Interpolation::Linear)
	{
		float v1 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f),		// (0,0)
			  v2 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (1,0)
			  v3 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (0,1)
			  v4 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f);		// (1,1)

		float t1 = Interpolation::getInstance()->linearInterpolate(v1, v2, x - (float)x_int),	// Interpolate between (0,0) - (1,0) @ x axis
			  t2 = Interpolation::getInstance()->linearInterpolate(v3, v4, x - (float)x_int);	// Interpolate between (0,1) - (1,1) @ x axis

		result = Interpolation::getInstance()->linearInterpolate(t1, t2, y - (float)y_int);		// Interpolate values @ Y axis
	}

	if (iType == Interpolation::Cosine)
	{
		float v1 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f),		// (0,0)
			  v2 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (1,0)
			  v3 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (0,1)
			  v4 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f);		// (1,1)

		float t1 = Interpolation::getInstance()->cosineInterpolate(v1, v2, x - (float)x_int),	// Interpolate between (0,0) - (1,0) @ x axis
			  t2 = Interpolation::getInstance()->cosineInterpolate(v3, v4, x - (float)x_int);	// Interpolate between (0,1) - (1,1) @ x axis

		result = Interpolation::getInstance()->cosineInterpolate(t1, t2, y - (float)y_int);		// Interpolate values @ Y axis
	}

	if (iType == Interpolation::Cubic)
	{
			  // Interpolate between (-1,-1) - (2,-1) @ X axis
		float v1 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f),		// (-1,-1)
			  v2 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (0,-1)
			  v3 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f),		// (1,-1)
			  v4 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (2,-1)

			  v5 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (-1,0)
			  v6 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f),		// (0,0)
			  v7 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (1,0)
			  v8 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f),		// (2,0)

			  v9 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f),		// (-1,1)
			  v10 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (0,1)
			  v11 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f),		// (1,1)
			  v12 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (2,1)

			  v13 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (-1,2)
			  v14 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f),		// (0,2)
			  v15 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (1,2)
			  v16 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f);		// (2,2)

		float t1 = Interpolation::getInstance()->cubicInterpolate(v1, // (-1,-1)
																  v2, // (0,-1)
																  v3, // (1,-1)
																  v4, // (2,-1)
																  x - (float)x_int),
			  // Interpolate between (-1,0) - (2,0) @ X axis
			  t2 = Interpolation::getInstance()->cubicInterpolate(v5, // (-1,0)
																  v6, // (0,0)
																  v7, // (1,0)
																  v8, // (2,0)
																  x - (float)x_int),
			  // Interpolate between (-1,1) - (2,1) @ X axis
			  t3 = Interpolation::getInstance()->cubicInterpolate(v9, // (-1,1)
																  v10, // (0,1)
																  v11, // (1,1)
																  v12, // (2,1)
																  x - (float)x_int),
			  // Interpolate between (-1,2) - (2,2) @ X axis
			  t4 = Interpolation::getInstance()->cubicInterpolate(v13, // (-1,2)
																  v14, // (0,2)
																  v15, // (1,2)
																  v16, // (2,2)
																  x - (float)x_int);
			
		result = Interpolation::getInstance()->cubicInterpolate(t1, t2, t3, t4, y - (float)y_int); 		// Interpolate values @ Y axis
	}

	if (iType == Interpolation::SmoothStep)
	{
		float v1 = vec2dotProduct(grad00, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 0.0f),		// (0,0)
			  v2 = vec2dotProduct(grad10, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 0.0f),		// (1,0)
			  v3 = vec2dotProduct(grad01, glm::vec2((x - (float)x_int) - 0.0f, (y - (float)y_int)) - 1.0f),		// (0,1)
			  v4 = vec2dotProduct(grad11, glm::vec2((x - (float)x_int) - 1.0f, (y - (float)y_int)) - 1.0f);		// (1,1)

		float t1 = Interpolation::getInstance()->smoothStep(v1, v2, x - (float)x_int),	// Interpolate between (0,0) - (1,0) @ x axis
			  t2 = Interpolation::getInstance()->smoothStep(v3, v4, x - (float)x_int);	// Interpolate between (0,1) - (1,1) @ x axis

		result = Interpolation::getInstance()->smoothStep(t1, t2, y - (float)y_int);		// Interpolate values @ Y axis
	}

	return result;
}

GeneratorStatus PerlinNoise2D::makeNoise(matrix<float> *heightMap)
{
	GeneratorStatus m_status = GeneratorStatus::GEN_NO_ERROR;

	if (!heightMap)
		m_status = GeneratorStatus::GEN_NULL_POINTER;
	else
	if ((this->m_heightMap->size1() < heightMap->size1()) ||
		(this->m_heightMap->size2() < heightMap->size2()))
		m_status = GeneratorStatus::GEN_SIZE_TOO_SMALL;
	else
	{
		uint16_t start_i = (uint16_t)((this->m_heightMap->size1() - heightMap->size1()) / 2.0f),
				 start_j = (uint16_t)((this->m_heightMap->size2() - heightMap->size2()) / 2.0f);

		for (uint16_t i = 0; i < heightMap->size1(); i++)
			for (uint16_t j = 0; j < heightMap->size2(); j++)
				(*heightMap)(i, j) = (((*this->m_heightMap)(i + start_i, j + start_j) - this->m_minVal) / (float)(this->m_maxVal - this->m_minVal)) - (this->m_isSigned?0.5f:0.0f);
	}

	return m_status;
}
